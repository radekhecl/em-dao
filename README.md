# EM Dao

The lightweight data access layer for working with big data. The main features are:

- ORM for immutable objects
- Super speed even with a lot of data
- The simple and unified way of filtering
- Build-in in-memory database

## Dependency Details

The project uses a Maven build system and binaries are deployed in The Central Repository.
Dependency coordinates are the following.
```
<dependency>
    <groupId>com.enterprisemath</groupId>
    <artifactId>em-dao</artifactId>
    <version>4.3.2</version>
</dependency>
```

## Licence

The project is licensed under MIT license.
Simply said, do whatever you want and don't blame me if anything goes wrong.
If you use this library and you find it useful, please drop me a message.

# Features

- Generic entity data access
- Object data access
