package com.enterprisemath.dao;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Test casefor in memory database. .
 *
 * @author radek.hecl
 */
public class InMemoryRawDaoTest extends AbstractRawDaoTest {

    /**
     *
     * Creates new instance.
     */
    public InMemoryRawDaoTest() {
    }

    @Override
    protected RawDao getDao() {
        return InMemoryRawDao.create();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
