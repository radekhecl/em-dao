package com.enterprisemath.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSourceFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransaction;

/**
 * Application for testing performance of insertion of big entities into the database.
 *
 * @author radek.hecl
 */
public class PostgreSqlInsertPerformanceTestApp {

    /**
     * Main function.
     *
     * @param args arguments
     * @throws Exception in case of error
     */
    public static void main(String[] args) throws Exception {
        RawDao dao = getDao();
        List<RawRecord> records = new ArrayList<>();
        for (int i = 0; i < 1000000; ++i) {
            RawRecord rec = new RawRecord.Builder().
                    setField("id", "00000000-0000-0000-0000-" + StringUtils.leftPad("" + i, 12, "0")).
                    setNullField("null_field").
                    setField("bool_field", true).
                    setField("int_field", i).
                    setField("long_field", Long.valueOf(i)).
                    setField("double_field", Double.valueOf(i)).
                    setField("string_field", "string " + i).
                    setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00").plusSeconds(i)).
                    setField("data_field", new byte[]{(byte) i, (byte) (i + 1), (byte) (i + 2)}).
                    build();
            records.add(rec);
        }

        long start = System.currentTimeMillis();
        dao.insert("records", records, null);
        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("---");
        System.out.println("Duration: " + duration + "; referenceDuration = 45095");
        System.out.println("---");
        System.out.println("JOB DONE!!!");
    }

    /**
     * Returns dao.
     *
     * @return dao
     */
    private static RawDao getDao() {
        try {
            Properties properties = new Properties();
            properties.setProperty("driverClassName", "org.postgresql.Driver");
            properties.setProperty("url", "jdbc:postgresql://localhost:5432/em-test?stringtype=unspecified");
            properties.setProperty("username", "em");
            properties.setProperty("password", "Password1");
            properties.setProperty("validationQuery", "select version();");
            properties.setProperty("defaultAutoCommit", "true");

            DataSource dataSource = BasicDataSourceFactory.createDataSource(properties);
            TransactionFactory transactionFactory = new TransactionFactory() {
                @Override
                public void setProperties(Properties arg0) {
                }

                @Override
                public Transaction newTransaction(DataSource ds, TransactionIsolationLevel desiredLevel, boolean desiredAutoCommit) {
                    return new JdbcTransaction(ds, desiredLevel, true);
                }

                @Override
                public Transaction newTransaction(Connection connection) {
                    return new JdbcTransaction(connection);
                }
            };
            Environment environment = new Environment("unit-test", transactionFactory, dataSource);
            Configuration configuration = new Configuration(environment);
            configuration.setDatabaseId("em-test");
            configuration.addMapper(PostgreSqlRawMapper.class);
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);

            List<String> setupScripts = Arrays.asList(
                    "drop table if exists records;",
                    "create table records (id uuid not null, null_field integer, bool_field boolean not null, " +
                    "int_field integer not null, long_field bigint not null, double_field float(53) not null, " +
                    "string_field text not null, datetime_field timestamp not null, data_field bytea not null, " +
                    "unique (id));");
            Connection con = null;
            Statement stm = null;
            try {
                con = dataSource.getConnection();
                stm = con.createStatement();
                for (String sql : setupScripts) {
                    stm.execute(sql);
                }
                stm.close();
                con.close();
            } finally {
                try {
                    if (stm != null && !stm.isClosed()) {
                        stm.close();
                    }
                } catch (SQLException e) {
                    // ignore
                }
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException e) {
                    // ignore
                }
            }

            PostgreSqlRawDao res = new PostgreSqlRawDao.Builder().
                    setSqlSessionFactory(sqlSessionFactory).
                    build();
            return res;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
