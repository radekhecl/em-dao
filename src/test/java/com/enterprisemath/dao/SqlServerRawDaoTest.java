package com.enterprisemath.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSourceFactory;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransaction;

/**
 * Test case for Microsoft SQL Server.
 *
 * @author radek.hecl
 */
public class SqlServerRawDaoTest extends AbstractRawDaoTest {

    /**
     * Creates new instance.
     *
     */
    public SqlServerRawDaoTest() {
    }

    @Override
    protected RawDao getDao() {
        try {
            Properties properties = new Properties();
            properties.setProperty("driverClassName", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            properties.setProperty("url", "jdbc:sqlserver://localhost:1433;databaseName=em-test");
            properties.setProperty("username", "em-test");
            properties.setProperty("password", "Password1");
            //properties.setProperty("validationQuery", "select version();");
            properties.setProperty("defaultAutoCommit", "true");
            
            DataSource dataSource = BasicDataSourceFactory.createDataSource(properties);
            TransactionFactory transactionFactory = new TransactionFactory() {
                @Override
                public void setProperties(Properties arg0) {
                }

                @Override
                public Transaction newTransaction(DataSource ds, TransactionIsolationLevel desiredLevel, boolean desiredAutoCommit) {
                    return new JdbcTransaction(ds, desiredLevel, true);
                }

                @Override
                public Transaction newTransaction(Connection connection) {
                    return new JdbcTransaction(connection);
                }
            };
            Environment environment = new Environment("unit-test", transactionFactory, dataSource);
            Configuration configuration = new Configuration(environment);
            configuration.setDatabaseId("em-test");
            configuration.addMapper(SqlServerRawMapper.class);
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);

            List<String> setupScripts = Arrays.asList(
                    "drop table if exists records;",
                    "create table records (id uniqueidentifier not null, null_field integer, bool_field bit not null, " +
                    "int_field integer not null, long_field bigint not null, double_field float not null, " +
                    "string_field varchar(6000) not null, datetime_field datetime2 not null, data_field varbinary(5000) not null, " +
                    "unique (id));");
            Connection con = null;
            Statement stm = null;
            try {
                con = dataSource.getConnection();
                stm = con.createStatement();
                for (String sql : setupScripts) {
                    stm.execute(sql);
                }
                stm.close();
                con.close();
            } finally {
                try {
                    if (stm != null && !stm.isClosed()) {
                        stm.close();
                    }
                } catch (SQLException e) {
                    // ignore
                }
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                } catch (SQLException e) {
                    // ignore
                }
            }

            SqlServerRawDao res = new SqlServerRawDao.Builder().
                    setSqlSessionFactory(sqlSessionFactory).
                    build();
            return res;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
