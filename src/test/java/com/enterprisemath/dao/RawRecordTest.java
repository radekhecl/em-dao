package com.enterprisemath.dao;

import java.time.LocalDateTime;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Test case for raw record.
 *
 * @author radek.hecl
 */
public class RawRecordTest {

    /**
     * Creates new instance.
     */
    public RawRecordTest() {
    }

    /**
     * Tests equals and hash code methods.
     */
    @Test
    public void testEqualsAndHashCode() {
        RawRecord rec1a = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                build();
        RawRecord rec1b = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                build();

        RawRecord rec2 = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000002").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                build();
        RawRecord rec3 = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                build();
        RawRecord rec4 = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                setNullField("extra").
                build();

        assertTrue(rec1a.equals(rec1b));
        assertTrue(rec1b.equals(rec1a));
        assertTrue(rec1a.hashCode() == rec1b.hashCode());

        assertFalse(rec1a.equals(null));
        assertFalse(rec1a.equals(rec2));
        assertFalse(rec2.equals(rec1a));
        assertFalse(rec1a.equals(rec3));
        assertFalse(rec4.equals(rec1a));
        assertFalse(rec1a.equals(rec4));
        assertFalse(rec4.equals(rec1a));

        assertTrue(rec1a.hashCode() != rec2.hashCode());
        assertTrue(rec1a.hashCode() != rec3.hashCode());
        assertTrue(rec1a.hashCode() != rec4.hashCode());
        assertTrue(rec2.hashCode() != rec3.hashCode());
        assertTrue(rec2.hashCode() != rec4.hashCode());
        assertTrue(rec3.hashCode() != rec4.hashCode());
    }

    /**
     * Tests record update.
     */
    @Test
    public void testUpdate() {
        RawRecord rec1a = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 10).
                setField("long_field", 100L).
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                build();
        RawRecord rec1b = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setField("null_field", 1).
                setField("bool_field", false).
                setField("int_field", 15).
                setNullField("long_field").
                setField("double_field", 10.0d).
                setField("string_field", "string 1").
                setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                setField("data_field", new byte[]{1, 2, 3}).
                build();
        RawRecord rec1c = new RawRecord.Builder().
                setField("id", "00000000-0000-0000-0000-000000000001").
                setField("null_field", 1).
                setField("bool_field", false).
                setField("int_field", 15).
                setNullField("long_field").
                setField("double_field", 15.0d).
                setField("string_field", "1 string").
                setField("datetime_field", LocalDateTime.parse("2019-12-10T11:30:10")).
                setField("data_field", new byte[]{4, 5, 6}).
                build();
        RawUpdate update1 = new RawUpdate.Builder().
                setField("null_field", 1).
                setField("bool_field", false).
                setField("int_field", 15).
                setNullField("long_field").
                build();
        RawUpdate update2 = new RawUpdate.Builder().
                setField("double_field", 15.0d).
                setField("string_field", "1 string").
                setField("datetime_field", LocalDateTime.parse("2019-12-10T11:30:10")).
                setField("data_field", new byte[]{4, 5, 6}).
                build();

        assertEquals(rec1b, rec1a.update(update1));
        assertEquals(rec1c, rec1a.update(update1).update(update2));
    }
}
