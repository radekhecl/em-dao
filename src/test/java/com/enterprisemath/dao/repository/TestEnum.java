package com.enterprisemath.dao.repository;

/**
 * Sample enumeration.
 *
 * @author radek.hecl
 */
public enum TestEnum {

    /**
     * Option 1.
     */
    OPTION_1,
    /**
     * Option 2.
     */
    OPTION_2

}
