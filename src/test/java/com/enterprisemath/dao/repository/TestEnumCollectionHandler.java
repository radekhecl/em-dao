package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.List;
import java.util.Set;

/**
 * Test class which holds collection of enum classes.
 *
 * @author radek.hecl
 */
public class TestEnumCollectionHandler {

    /**
     * Identifier.
     */
    private String id;

    /**
     * Enum list value.
     */
    private List<TestEnum> enumListValue;

    /**
     * Enum set value.
     */
    private Set<TestEnum> enumSetValue;

    /**
     * Creates new instance.
     */
    private TestEnumCollectionHandler() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNullCollection(enumListValue, "enumListValue cannot have null element");
        Guard.notNullCollection(enumSetValue, "enumSetValue cannot have null element");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns enum list value.
     *
     * @return enum list value
     */
    public List<TestEnum> getEnumListValue() {
        return enumListValue;
    }

    /**
     * Returns enum set value.
     *
     * @return enum set value
     */
    public Set<TestEnum> getEnumSetValue() {
        return enumSetValue;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param enumListValue enum list value
     * @param enumSetValue enum set value
     * @return crated object
     */
    public static TestEnumCollectionHandler create(String id, List<TestEnum> enumListValue, Set<TestEnum> enumSetValue) {
        TestEnumCollectionHandler res = new TestEnumCollectionHandler();
        res.id = id;
        res.enumListValue = Dut.copyImmutableList(enumListValue);
        res.enumSetValue = Dut.copyImmutableSet(enumSetValue);
        res.guardInvariants();
        return res;
    }
}
