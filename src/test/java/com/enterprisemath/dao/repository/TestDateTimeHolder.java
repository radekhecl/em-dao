package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Test class which holds date and time classes.
 *
 * @author radek.hecl
 */
public class TestDateTimeHolder {

    /**
     * Identifier.
     */
    private String id;

    /**
     * Instant value.
     */
    private Instant instantValue;

    /**
     * Local date time value.
     */
    private LocalDateTime localDateTimeValue;

    /**
     * Date value.
     */
    private Date dateValue;

    /**
     * Creates new instance.
     */
    private TestDateTimeHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNull(instantValue, "instantValue cannot be null");
        Guard.notNull(localDateTimeValue, "localDateTimeValue cannot be null");
        Guard.notNull(dateValue, "dateValue cannot be null");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns instant value.
     *
     * @return instant value
     */
    public Instant getInstantValue() {
        return instantValue;
    }

    /**
     * Returns local date time value.
     *
     * @return local date time value
     */
    public LocalDateTime getLocalDateTimeValue() {
        return localDateTimeValue;
    }

    /**
     * Returns date value.
     *
     * @return date value
     */
    public Date getDateValue() {
        return Dut.copyDate(dateValue);
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param instantValue instant value
     * @param localDateTimeValue local date time value
     * @param dateValue date value
     * @return crated object
     */
    public static TestDateTimeHolder create(String id, Instant instantValue, LocalDateTime localDateTimeValue, Date dateValue) {
        TestDateTimeHolder res = new TestDateTimeHolder();
        res.id = id;
        res.instantValue = instantValue;
        res.localDateTimeValue = localDateTimeValue;
        res.dateValue = Dut.copyDate(dateValue);
        res.guardInvariants();
        return res;
    }
}
