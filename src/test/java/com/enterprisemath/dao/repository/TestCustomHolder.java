package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.ANumber;
import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;

/**
 * Test class which holds the custom class.
 *
 * @author radek.hecl
 */
public class TestCustomHolder {

    /**
     * Identifier.
     */
    private String id;

    /**
     * Number.
     */
    private ANumber number;

    /**
     * Creates new instance.
     */
    private TestCustomHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNull(number, "number cannot be empty");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns number.
     *
     * @return number
     */
    public ANumber getNumber() {
        return number;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param number number
     * @return crated instance
     */
    public static TestCustomHolder create(String id, ANumber number) {
        TestCustomHolder res = new TestCustomHolder();
        res.id = id;
        res.number = number;
        res.guardInvariants();
        return res;
    }
}
