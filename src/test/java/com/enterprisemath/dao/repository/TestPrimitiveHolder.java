package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;

/**
 * Test class which holds just primitive types.
 *
 * @author radek.hecl
 */
public class TestPrimitiveHolder {

    /**
     * Added static field to make sure it is ignored.
     */
    public static String STATIC_FIELD_TO_BE_IGNORED = "Hello World";

    /**
     * Identifier.
     */
    private String id;

    /**
     * Boolean value.
     */
    private boolean boolValue;

    /**
     * Integer value.
     */
    private int intValue;

    /**
     * Long value.
     */
    private long longValue;

    /**
     * Double value.
     */
    private double doubleValue;

    /**
     * Creates new instance.
     */
    private TestPrimitiveHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNegative(intValue, "intValue cannot be negative");
        Guard.notNegative(longValue, "longValue cannot be negative");
        Guard.beTrue(doubleValue >= 0.0 && doubleValue <= 1.0, "double value must be in [0, 1]");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns boolean value.
     *
     * @return boolean value
     */
    public boolean isBoolValue() {
        return boolValue;
    }

    /**
     * Returns integer value.
     *
     * @return integer value
     */
    public int getIntValue() {
        return intValue;
    }

    /**
     * Returns long value.
     *
     * @return long value
     */
    public long getLongValue() {
        return longValue;
    }

    /**
     * Returns double value.
     *
     * @return double value
     */
    public double getDoubleValue() {
        return doubleValue;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param boolValue boolean value
     * @param intValue int value
     * @param longValue long value
     * @param doubleValue double value
     * @return crated instance
     */
    public static TestPrimitiveHolder create(String id, boolean boolValue, int intValue, long longValue, double doubleValue) {
        TestPrimitiveHolder res = new TestPrimitiveHolder();
        res.id = id;
        res.boolValue = boolValue;
        res.intValue = intValue;
        res.longValue = longValue;
        res.doubleValue = doubleValue;
        res.guardInvariants();
        return res;
    }
}
