package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

/**
 * Test class which holds collection classes.
 *
 * @author radek.hecl
 */
public class TestCollectionHolder {

    /**
     * Identifier.
     */
    private String id;

    /**
     * String list value.
     */
    private List<String> stringListValue;

    /**
     * String set value.
     */
    private Set<String> stringSetValue;

    /**
     * Sorted string set value.
     */
    private SortedSet<String> sortedStringSetValue;

    /**
     * String map value.
     */
    private Map<String, String> stringMapValue;

    /**
     * Sorted string map value.
     */
    private SortedMap<String, String> sortedStringMapValue;

    /**
     * Creates new instance.
     */
    private TestCollectionHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notEmptyStringCollection(stringListValue, "stringListValue cannot have empty element");
        Guard.notEmptyStringCollection(stringSetValue, "stringSetValue cannot have empty element");
        Guard.notEmptyStringCollection(sortedStringSetValue, "sortedStringSetValue cannot have empty element");
        Guard.notEmptyNullMap(stringMapValue, "stringMapValue cannot have empty key or null value");
        Guard.notEmptyNullMap(sortedStringMapValue, "sortedStringMapValue cannot have empty key or null value");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns string list value.
     *
     * @return string list value
     */
    public List<String> getStringListValue() {
        return stringListValue;
    }

    /**
     * Returns string set value.
     *
     * @return string set value
     */
    public Set<String> getStringSetValue() {
        return stringSetValue;
    }

    /**
     * Returns sorted string set value.
     *
     * @return sorted string set valiue
     */
    public SortedSet<String> getSortedStringSetValue() {
        return sortedStringSetValue;
    }

    /**
     * Returns string map value.
     *
     * @return string map value
     */
    public Map<String, String> getStringMapValue() {
        return stringMapValue;
    }

    /**
     * Returns sorted string map value.
     *
     * @return sorted string map value
     */
    public SortedMap<String, String> getSortedStringMapValue() {
        return sortedStringMapValue;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param stringListValue string list value
     * @param stringSetValue string set value
     * @param sortedStringSetValue sorted string set value
     * @param stringMapValue string map value
     * @param sortedStringMapValue sorted string map value
     * @return crated object
     */
    public static TestCollectionHolder create(String id, List<String> stringListValue, Set<String> stringSetValue,
            SortedSet<String> sortedStringSetValue, Map<String, String> stringMapValue, SortedMap<String, String> sortedStringMapValue) {
        TestCollectionHolder res = new TestCollectionHolder();
        res.id = id;
        res.stringListValue = Dut.copyImmutableList(stringListValue);
        res.stringSetValue = Dut.copyImmutableSet(stringSetValue);
        res.sortedStringSetValue = Dut.copyImmutableSortedSet(sortedStringSetValue);
        res.stringMapValue = Dut.copyImmutableMap(stringMapValue);
        res.sortedStringMapValue = Dut.copyImmutableSortedMap(sortedStringMapValue);
        res.guardInvariants();
        return res;
    }
}
