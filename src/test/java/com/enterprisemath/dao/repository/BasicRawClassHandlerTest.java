package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.RawRecord;
import com.enterprisemath.dao.RawUpdate;
import com.enterprisemath.utils.ANumber;
import com.enterprisemath.utils.Dates;
import com.enterprisemath.utils.Dut;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Handler test case.
 *
 * @author radek.hecl
 */
public class BasicRawClassHandlerTest {

    /**
     * Creates new instance.
     */
    public BasicRawClassHandlerTest() {
    }

    /**
     * Tests camel to underscore conversion.
     */
    @Test
    public void testCamelToUnderscore() {
        // first character is capital
        assertEquals("test", BasicRawClassHandler.camelToUnderscore("Test"));
        assertEquals("test_v2", BasicRawClassHandler.camelToUnderscore("TestV2"));
        assertEquals("test2_version1", BasicRawClassHandler.camelToUnderscore("Test2Version1"));
        assertEquals("test_x_y_z", BasicRawClassHandler.camelToUnderscore("TestXYZ"));
        assertEquals("test_x_y_zalpha", BasicRawClassHandler.camelToUnderscore("TestXYZalpha"));
        // first character is lower csse
        assertEquals("test", BasicRawClassHandler.camelToUnderscore("test"));
        assertEquals("test_v2", BasicRawClassHandler.camelToUnderscore("testV2"));
        assertEquals("test2_version1", BasicRawClassHandler.camelToUnderscore("test2Version1"));
        assertEquals("test_x_y_z", BasicRawClassHandler.camelToUnderscore("testXYZ"));
        assertEquals("test_x_y_zalpha", BasicRawClassHandler.camelToUnderscore("testXYZalpha"));
    }

    /**
     * Tests underscore to camel conversion.
     */
    @Test
    public void testUnderscoreToCamel() {
        assertEquals("test", BasicRawClassHandler.underscoreToCamel("test"));
        assertEquals("testV2", BasicRawClassHandler.underscoreToCamel("test_v2"));
        assertEquals("test2Version1", BasicRawClassHandler.underscoreToCamel("test2_version1"));
        assertEquals("testXYZ", BasicRawClassHandler.underscoreToCamel("test_x_y_z"));
        assertEquals("testXYZalpha", BasicRawClassHandler.underscoreToCamel("test_x_y_zalpha"));
    }

    /**
     * Tests singular to plural conversion.
     */
    @Test
    public void testSingularToPlural() {
        assertEquals("customers", BasicRawClassHandler.singularToPlural("customer"));
        assertEquals("customer_data", BasicRawClassHandler.singularToPlural("customer_data"));
        assertEquals("customer_classes", BasicRawClassHandler.singularToPlural("customer_class"));
        assertEquals("application_statuses", BasicRawClassHandler.singularToPlural("application_status"));
        assertEquals("authorities", BasicRawClassHandler.singularToPlural("authority"));
        assertEquals("my_properties", BasicRawClassHandler.singularToPlural("my_property"));
        assertEquals("public_keys", BasicRawClassHandler.singularToPlural("public_key"));
    }

    /**
     * Tests for retrieving collection out of the class.
     */
    @Test
    public void testGetCollection() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create();

        assertEquals("test_primitive_holders", hndl.getCollection(TestPrimitiveHolder.class));
        // second same test is here to run a round through the cached instance
        assertEquals("test_primitive_holders", hndl.getCollection(TestPrimitiveHolder.class));
    }

    /**
     * Tests retrieving the id field.
     */
    @Test
    public void testGetIdFieldValue() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create();

        assertEquals("ID-1", hndl.getIdFieldValue(TestPrimitiveHolder.create("ID-1", true, 1, 100L, 1.0d)));
        assertEquals("ID-1", hndl.getIdFieldValue(TestBoxedHolder.create("ID-1", true, 1, 100L, 1.0d)));
        assertEquals("ID-1", hndl.getIdFieldValue(new TestBuilderHolder.Builder().
                setId("ID-1").
                setName("John").
                build()));
        assertEquals("ID-1", hndl.getIdFieldValue(TestCollectionHolder.create("ID-1", Dut.list("list-1", "list-2"),
                Dut.set("set-1", "set-2"), Dut.sortedSet("sset-1", "sset-2"),
                Dut.map("mk-1", "mv-1", "mk-2", "mv-2"), Dut.sortedMap("smk-1", "smv-1", "smk-2", "smv-2"))));
        assertEquals("ID-1", hndl.getIdFieldValue(TestEnumCollectionHandler.create("ID-1", Dut.list(TestEnum.OPTION_1, TestEnum.OPTION_2),
                Dut.set(TestEnum.OPTION_1))));
        assertEquals("ID-1", hndl.getIdFieldValue(TestDateTimeHolder.create("ID-1", Instant.parse("2019-01-01T20:00:00.000Z"),
                LocalDateTime.parse("2019-01-01T21:00:00.000"), Dates.createDate(2019, Month.JANUARY, 2))));
        assertEquals("ID-1", hndl.getIdFieldValue(TestEnumHolder.create("ID-1", TestEnum.OPTION_1)));
        assertEquals("ID-1", hndl.getIdFieldValue(TestCustomHolder.create("ID-1", ANumber.create("100"))));
    }

    /**
     * Tests retrieving the fields to be used in raw record out of the class.
     */
    @Test
    public void testGetRawFields() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create();

        assertEquals(Dut.set("id", "bool_value", "int_value", "long_value", "double_value"), hndl.getRawFields(TestPrimitiveHolder.class));
        assertEquals(Dut.set("id", "bool_value", "int_value", "long_value", "double_value"), hndl.getRawFields(TestBoxedHolder.class));
        assertEquals(Dut.set("id", "name"), hndl.getRawFields(TestBuilderHolder.class));
        assertEquals(Dut.set("id", "string_list_value", "string_set_value", "sorted_string_set_value",
                "string_map_value", "sorted_string_map_value"), hndl.getRawFields(TestCollectionHolder.class));
        assertEquals(Dut.set("id", "enum_list_value", "enum_set_value"),
                hndl.getRawFields(TestEnumCollectionHandler.class));
        assertEquals(Dut.set("id", "instant_value", "local_date_time_value", "date_value"), hndl.getRawFields(TestDateTimeHolder.class));
        assertEquals(Dut.set("id", "enum_value"), hndl.getRawFields(TestEnumHolder.class));
        assertEquals(Dut.set("id", "number"), hndl.getRawFields(TestCustomHolder.class));
    }

    /**
     * Tests conversion to the raw fields.
     */
    @Test
    public void testToRawFields() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create();

        assertEquals(Dut.set("id", "int_value", "another_field"), hndl.toRawFields(TestBoxedHolder.class, Dut.set("id", "intValue", "anotherField")));
        assertEquals(Dut.set("created_timestamp"), hndl.toRawFields(Object.class, Dut.set("createdTimestamp")));
    }

    /**
     * Tests filter conversion.
     */
    @Test
    public void testToRawFilter() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create();

        Filter input = null;
        Filter expected = null;

        input = new Filter.Builder().
                addEqualCriterium("nullValue", null).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("null_value", null).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("boolValue", true).
                addAscendantOrder("createdTimestamp").
                setOffset(0).
                setLimit(100).
                build();
        expected = new Filter.Builder().
                addEqualCriterium("bool_value", true).
                addAscendantOrder("created_timestamp").
                setOffset(0).
                setLimit(100).
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("intValue", 1).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("int_value", 1).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("longValue", 100L).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("long_value", 100L).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("doubleValue", 1.0d).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("double_value", 1.0d).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("stringValue", "hello").
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("string_value", "hello").
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("enumValue", TestEnum.OPTION_1).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("enum_value", "OPTION_1").
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("enumColValue", Dut.list(TestEnum.OPTION_1, TestEnum.OPTION_2)).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("enum_col_value", Dut.list("OPTION_1", "OPTION_2")).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));

        input = new Filter.Builder().
                addEqualCriterium("instantValue", Instant.parse("2019-01-01T12:00:00.000Z")).
                addEqualCriterium("localDateTimeValue", LocalDateTime.parse("2019-01-01T13:00:00.000")).
                addEqualCriterium("dateValue", Dates.createTime(2019, Month.JANUARY, 1, 14, 0, 0)).
                addAscendantOrder("createdTimestamp").
                build();
        expected = new Filter.Builder().
                addEqualCriterium("instant_value", LocalDateTime.parse("2019-01-01T12:00:00.000")).
                addEqualCriterium("local_date_time_value", LocalDateTime.parse("2019-01-01T13:00:00.000")).
                addEqualCriterium("date_value", LocalDateTime.parse("2019-01-01T14:00:00.000")).
                addAscendantOrder("created_timestamp").
                build();
        assertEquals(expected, hndl.toRawFilter(TestPrimitiveHolder.class, input));
    }

    /**
     * Tests object to raw record conversion.
     */
    @Test
    public void testToRawRecord() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create(Dut.map(ANumber.class, ANumberRawTypeHandler.create()));
        Object input = null;
        RawRecord expected = null;

        input = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 1.0d);
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestBoxedHolder.create("ID-1", true, 1, 100L, 1.0d);
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = new TestBuilderHolder.Builder().
                setId("ID-1").
                setName("John").
                build();
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("name", "John").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestCollectionHolder.create("ID-1", Dut.list("list-1", "list-2"),
                Dut.set("set-1", "set-2"), Dut.sortedSet("sset-1", "sset-2"),
                Dut.map("mk-1", "mv-1", "mk-2", "mv-2"), Dut.sortedMap("smk-1", "smv-1", "smk-2", "smv-2"));
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("string_list_value", "[\"list-1\",\"list-2\"]").
                setField("string_set_value", "[\"set-1\",\"set-2\"]").
                setField("sorted_string_set_value", "[\"sset-1\",\"sset-2\"]").
                setField("string_map_value", "{\"mk-1\":\"mv-1\",\"mk-2\":\"mv-2\"}").
                setField("sorted_string_map_value", "{\"smk-1\":\"smv-1\",\"smk-2\":\"smv-2\"}").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestEnumCollectionHandler.create("ID-1", Dut.list(TestEnum.OPTION_1, TestEnum.OPTION_2),
                Dut.set(TestEnum.OPTION_1));
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("enum_list_value", "[\"OPTION_1\",\"OPTION_2\"]").
                setField("enum_set_value", "[\"OPTION_1\"]").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestDateTimeHolder.create("ID-1", Instant.parse("2019-01-01T20:00:00.000Z"),
                LocalDateTime.parse("2019-01-01T21:00:00.000"), Dates.createDate(2019, Month.JANUARY, 2));
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("instant_value", LocalDateTime.parse("2019-01-01T20:00:00.000")).
                setField("local_date_time_value", LocalDateTime.parse("2019-01-01T21:00:00.000")).
                setField("date_value", LocalDateTime.parse("2019-01-02T00:00:00.000")).
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestEnumHolder.create("ID-1", TestEnum.OPTION_1);
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("enum_value", "OPTION_1").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        input = TestCustomHolder.create("ID-1", ANumber.create("100"));
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("number", "100").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Collections.emptyMap()));

        // with extra data
        input = TestEnumHolder.create("ID-1", TestEnum.OPTION_1);
        expected = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("enum_value", "OPTION_1").
                setField("data", new byte[]{1, 2, 3}).
                setNullField("null_field").
                build();
        assertEquals(expected, hndl.toRawRecord(input, Dut.map("data", (Object) (new byte[]{1, 2, 3}), "nullField", null)));
    }

    /**
     * Tests conversion to update.
     */
    @Test
    public void testToRawUpdate() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create(Dut.map(ANumber.class, ANumberRawTypeHandler.create()));
        Object input = null;
        RawUpdate expected = null;

        // tests full object conversion
        input = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 1.0d);
        expected = new RawUpdate.Builder().
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestBoxedHolder.create("ID-1", true, 1, 100L, 1.0d);
        expected = new RawUpdate.Builder().
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = new TestBuilderHolder.Builder().
                setId("ID-1").
                setName("John").
                build();
        expected = new RawUpdate.Builder().
                setField("name", "John").
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestCollectionHolder.create("ID-1", Dut.list("list-1", "list-2"),
                Dut.set("set-1", "set-2"), Dut.sortedSet("sset-1", "sset-2"),
                Dut.map("mk-1", "mv-1", "mk-2", "mv-2"), Dut.sortedMap("smk-1", "smv-1", "smk-2", "smv-2"));
        expected = new RawUpdate.Builder().
                setField("string_list_value", "[\"list-1\",\"list-2\"]").
                setField("string_set_value", "[\"set-1\",\"set-2\"]").
                setField("sorted_string_set_value", "[\"sset-1\",\"sset-2\"]").
                setField("string_map_value", "{\"mk-1\":\"mv-1\",\"mk-2\":\"mv-2\"}").
                setField("sorted_string_map_value", "{\"smk-1\":\"smv-1\",\"smk-2\":\"smv-2\"}").
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestEnumCollectionHandler.create("ID-1", Dut.list(TestEnum.OPTION_1, TestEnum.OPTION_2),
                Dut.set(TestEnum.OPTION_1));
        expected = new RawUpdate.Builder().
                setField("enum_list_value", "[\"OPTION_1\",\"OPTION_2\"]").
                setField("enum_set_value", "[\"OPTION_1\"]").
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestDateTimeHolder.create("ID-1", Instant.parse("2019-01-01T20:00:00.000Z"),
                LocalDateTime.parse("2019-01-01T21:00:00.000"), Dates.createDate(2019, Month.JANUARY, 2));
        expected = new RawUpdate.Builder().
                setField("instant_value", LocalDateTime.parse("2019-01-01T20:00:00.000")).
                setField("local_date_time_value", LocalDateTime.parse("2019-01-01T21:00:00.000")).
                setField("date_value", LocalDateTime.parse("2019-01-02T00:00:00.000")).
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestEnumHolder.create("ID-1", TestEnum.OPTION_1);
        expected = new RawUpdate.Builder().
                setField("enum_value", "OPTION_1").
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        input = TestCustomHolder.create("ID-1", ANumber.create("100"));
        expected = new RawUpdate.Builder().
                setField("number", "100").
                build();
        assertEquals(expected, hndl.toRawUpdate(input));

        // test with free fields
        input = Dut.map("enumValue", TestEnum.OPTION_1, "data", new byte[]{1, 2, 3}, "nullField", null);
        expected = new RawUpdate.Builder().
                setField("enum_value", "OPTION_1").
                setField("data", new byte[]{1, 2, 3}).
                setNullField("null_field").
                build();
        assertEquals(expected, hndl.toRawUpdate(TestEnumHolder.class, (Map<String, Object>) input));
    }

    /**
     * Tests conversion from raw record to the object.
     */
    @Test
    public void testToObject() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create(Dut.map(ANumber.class, ANumberRawTypeHandler.create()));
        RawRecord input = null;
        Object expected = null;

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        expected = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 1.0d);
        assertEquals(expected, hndl.toObject(TestPrimitiveHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        expected = TestBoxedHolder.create("ID-1", true, 1, 100L, 1.0d);
        assertEquals(expected, hndl.toObject(TestBoxedHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("name", "John").
                build();
        expected = new TestBuilderHolder.Builder().
                setId("ID-1").
                setName("John").
                build();
        assertEquals(expected, hndl.toObject(TestBuilderHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("string_list_value", "[\"list-1\",\"list-2\"]").
                setField("string_set_value", "[\"set-1\",\"set-2\"]").
                setField("sorted_string_set_value", "[\"sset-1\",\"sset-2\"]").
                setField("string_map_value", "{\"mk-1\":\"mv-1\",\"mk-2\":\"mv-2\"}").
                setField("sorted_string_map_value", "{\"smk-1\":\"smv-1\",\"smk-2\":\"smv-2\"}").
                build();
        expected = TestCollectionHolder.create("ID-1", Dut.list("list-1", "list-2"),
                Dut.set("set-1", "set-2"), Dut.sortedSet("sset-1", "sset-2"),
                Dut.map("mk-1", "mv-1", "mk-2", "mv-2"), Dut.sortedMap("smk-1", "smv-1", "smk-2", "smv-2"));
        assertEquals(expected, hndl.toObject(TestCollectionHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("enum_list_value", "[\"OPTION_1\",\"OPTION_2\"]").
                setField("enum_set_value", "[\"OPTION_1\"]").
                build();
        expected = TestEnumCollectionHandler.create("ID-1", Dut.list(TestEnum.OPTION_1, TestEnum.OPTION_2),
                Dut.set(TestEnum.OPTION_1));
        assertEquals(expected, hndl.toObject(TestEnumCollectionHandler.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("instant_value", LocalDateTime.parse("2019-01-01T20:00:00.000")).
                setField("local_date_time_value", LocalDateTime.parse("2019-01-01T21:00:00.000")).
                setField("date_value", LocalDateTime.parse("2019-01-02T00:00:00.000")).
                build();
        expected = TestDateTimeHolder.create("ID-1", Instant.parse("2019-01-01T20:00:00.000Z"),
                LocalDateTime.parse("2019-01-01T21:00:00.000"), Dates.createDate(2019, Month.JANUARY, 2));
        assertEquals(expected, hndl.toObject(TestDateTimeHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setNullField("instant_value").
                setNullField("local_date_time_value").
                setNullField("date_value").
                build();
        expected = TestDateTimeNullableHolder.create("ID-1", null, null, null);
        assertEquals(expected, hndl.toObject(TestDateTimeNullableHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("enum_value", "OPTION_1").
                build();
        expected = TestEnumHolder.create("ID-1", TestEnum.OPTION_1);
        assertEquals(expected, hndl.toObject(TestEnumHolder.class, input));

        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("number", "100").
                build();
        expected = TestCustomHolder.create("ID-1", ANumber.create("100"));
        assertEquals(expected, hndl.toObject(TestCustomHolder.class, input));
    }

    /**
     * Tests conversion to object fields.
     */
    @Test
    public void testToObjectFields() {
        BasicRawClassHandler hndl = BasicRawClassHandler.create(Dut.map(ANumber.class, ANumberRawTypeHandler.create()));
        RawRecord input = null;
        Object expected = null;

        // primitives
        input = new RawRecord.Builder().
                setField("id", "ID-1").
                setField("bool_value", true).
                setField("int_value", 1).
                setField("long_value", 100L).
                setField("double_value", 1.0d).
                build();
        expected = Dut.map("id", "ID-1", "boolValue", true, "intValue", 1, "longValue", 100L, "doubleValue", 1.0d);
        assertEquals(expected, hndl.toObjectFields(TestBoxedHolder.class, Dut.map("id", String.class, "boolValue", Boolean.class, "intValue",
                Integer.class, "longValue", Long.class, "doubleValue", Double.class), input));

        // string collections
        input = new RawRecord.Builder().
                setField("string_list_value", "[\"list-1\",\"list-2\"]").
                setField("string_set_value", "[\"set-1\",\"set-2\"]").
                setField("sorted_string_set_value", "[\"sset-1\",\"sset-2\"]").
                setField("string_map_value", "{\"mk-1\":\"mv-1\",\"mk-2\":\"mv-2\"}").
                setField("sorted_string_map_value", "{\"smk-1\":\"smv-1\",\"smk-2\":\"smv-2\"}").
                build();
        expected = Dut.map("stringListValue", Dut.list("list-1", "list-2"),
                "stringSetValue", Dut.set("set-1", "set-2"), "sortedStringSetValue", Dut.sortedSet("sset-1", "sset-2"),
                "stringMapValue", Dut.map("mk-1", "mv-1", "mk-2", "mv-2"), "sortedStringMapValue", Dut.sortedMap("smk-1", "smv-1", "smk-2", "smv-2"));
        assertEquals(expected, hndl.toObjectFields(TestBoxedHolder.class, Dut.map("stringListValue", List.class,
                "stringSetValue", Set.class, "sortedStringSetValue", SortedSet.class,
                "stringMapValue", Map.class, "sortedStringMapValue", SortedMap.class), input));

        // timestamps
        input = new RawRecord.Builder().
                setField("instant_value", LocalDateTime.parse("2019-01-01T20:00:00.000")).
                setField("local_date_time_value", LocalDateTime.parse("2019-01-01T21:00:00.000")).
                setField("date_value", LocalDateTime.parse("2019-01-02T00:00:00.000")).
                build();
        expected = Dut.map("instantValue", Instant.parse("2019-01-01T20:00:00.000Z"),
                "localDateTimeValue", LocalDateTime.parse("2019-01-01T21:00:00.000"),
                "dateValue", Dates.createDate(2019, Month.JANUARY, 2));
        assertEquals(expected, hndl.toObjectFields(TestBoxedHolder.class, Dut.map("instantValue", Instant.class,
                "localDateTimeValue", LocalDateTime.class,
                "dateValue", Date.class), input));

        // enumeration
        input = new RawRecord.Builder().
                setField("enum_value", "OPTION_1").
                build();
        expected = Dut.map("enumValue", TestEnum.OPTION_1);
        assertEquals(expected, hndl.toObjectFields(TestBoxedHolder.class, Dut.map("enumValue", TestEnum.class), input));

        // byte array
        input = new RawRecord.Builder().
                setField("data", new byte[]{1, 2, 3}).
                build();
        expected = new byte[]{1, 2, 3};
        assertArrayEquals((byte[]) expected, (byte[]) hndl.toObjectFields(TestBoxedHolder.class, Dut.map("data", byte[].class), input).get("data"));

        // custom
        input = new RawRecord.Builder().
                setField("number", "100").
                build();
        expected = Dut.map("number", ANumber.create("100"));
        assertEquals(expected, hndl.toObjectFields(TestBoxedHolder.class, Dut.map("number", ANumber.class), input));
    }

}
