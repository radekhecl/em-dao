package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;

/**
 * Test class which holds enum type.
 *
 * @author radek.hecl
 */
public class TestEnumHolder {

    /**
     * Identifier.
     */
    private String id;

    /**
     * Enum value.
     */
    private TestEnum enumValue;

    /**
     * Creates new instance.
     */
    private TestEnumHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNull(enumValue, "enumValue cannot be null");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the enum value.
     *
     * @return enum value
     */
    public TestEnum getEnumValue() {
        return enumValue;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param enumValue enum value
     * @return crated instance
     */
    public static TestEnumHolder create(String id, TestEnum enumValue) {
        TestEnumHolder res = new TestEnumHolder();
        res.id = id;
        res.enumValue = enumValue;
        res.guardInvariants();
        return res;
    }
}
