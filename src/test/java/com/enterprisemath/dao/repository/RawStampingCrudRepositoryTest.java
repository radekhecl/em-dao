package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Criterium;
import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.InMemoryRawDao;
import com.enterprisemath.utils.ConstantNowProvider;
import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.NowProvider;
import java.time.Instant;
import java.util.Arrays;
import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * Test case for repository.
 *
 * @author radek.hecl
 */
public class RawStampingCrudRepositoryTest {

    /**
     * Repository.
     */
    private CrudRepository repo;

    /**
     * Before timestamp.
     */
    private Instant before2 = Instant.parse("2022-06-01T12:00:00Z");

    /**
     * Before timestamp.
     */
    private Instant before1 = Instant.parse("2022-06-02T12:00:00Z");

    /**
     * Now timestamp.
     */
    private Instant now = Instant.parse("2022-06-03T12:00:00Z");

    /**
     *
     * Creates new instance.
     */
    public RawStampingCrudRepositoryTest() {
    }

    /**
     * Sets up the test environment.
     */
    @Before
    public void setyUp() {
        NowProvider nowProvider = ConstantNowProvider.create(now);
        repo = new RawStampingCrudRepository.Builder().
                setRawDao(InMemoryRawDao.create()).
                setHandler(BasicRawClassHandler.create()).
                setNowProvider(nowProvider).
                build();
    }

    /**
     * Tests insertion.
     */
    @Test
    public void testInsert() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestPrimitiveHolder p1 = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 0.1);
        TestPrimitiveHolder p2 = TestPrimitiveHolder.create("ID-2", false, 2, 200L, 0.2);
        TestPrimitiveHolder p3 = TestPrimitiveHolder.create("ID-3", true, 3, 300L, 0.2);

        assertEquals(p1, repo.insert(p1));
        assertEquals(p2, repo.insert(p2));
        assertEquals(p3, repo.insert(p3, Dut.map("data", new byte[]{1, 2, 3})));

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));
        assertArrayEquals(new byte[]{1, 2, 3}, repo.selectField(TestPrimitiveHolder.class, "ID-3", "data", byte[].class));
    }

    /**
     * Tests insertion with stamping.
     */
    @Test
    public void testInsert_stamp() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestStampedHolder p1 = TestStampedHolder.create("ID-1", 1, before2, before2, 1);
        TestStampedHolder p1s = TestStampedHolder.create("ID-1", 1, now, now, 1);
        TestStampedHolder p2 = TestStampedHolder.create("ID-2", 2, before1, before1, 1);
        TestStampedHolder p2s = TestStampedHolder.create("ID-2", 2, now, now, 1);
        TestStampedHolder p3 = TestStampedHolder.create("ID-3", 3, before1, before1, 1);
        TestStampedHolder p3s = TestStampedHolder.create("ID-3", 3, now, now, 1);

        assertEquals(p1s, repo.insert(p1));
        assertEquals(p2s, repo.insert(p2));
        assertEquals(p3s, repo.insert(p3, Dut.map("data", new byte[]{1, 2, 3})));

        assertEquals(Arrays.asList(p1s, p2s, p3s), repo.selectList(TestStampedHolder.class, flt));
        assertArrayEquals(new byte[]{1, 2, 3}, repo.selectField(TestStampedHolder.class, "ID-3", "data", byte[].class));
    }

    /**
     * Tests selection.
     */
    @Test
    public void testSelect() {
        Filter flt = null;
        TestPrimitiveHolder p1 = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 0.1);
        TestPrimitiveHolder p2 = TestPrimitiveHolder.create("ID-2", false, 2, 200L, 0.2);
        TestPrimitiveHolder p3 = TestPrimitiveHolder.create("ID-3", true, 3, 300L, 0.2);

        repo.insert(p1);
        repo.insert(p2, Dut.map("createdTs", Instant.parse("2020-01-01T12:21:11Z")));
        repo.insert(p3, Dut.map("data", new byte[]{1, 2, 3}));

        // list selection
        flt = new Filter.Builder().
                addDescendantOrder("id").
                build();
        assertEquals(Arrays.asList(p3, p2, p1), repo.selectList(TestPrimitiveHolder.class, flt));

        flt = new Filter.Builder().
                addEqualCriterium("boolValue", true).
                addDescendantOrder("id").
                build();
        assertEquals(Arrays.asList(p3, p1), repo.selectList(TestPrimitiveHolder.class, flt));

        flt = new Filter.Builder().
                addGreaterCriterium("intValue", 1).
                addDescendantOrder("id").
                build();
        assertEquals(Arrays.asList(p3, p2), repo.selectList(TestPrimitiveHolder.class, flt));

        // single selection
        assertEquals(p2, repo.selectSingle(TestPrimitiveHolder.class, "ID-2"));
        assertEquals(p2, repo.selectSingle(TestPrimitiveHolder.class, "ID-2", null));
        assertNull(repo.selectSingle(TestPrimitiveHolder.class, "ID-UNKNOWN", null));

        assertEquals(p2, repo.selectSingle(TestPrimitiveHolder.class, Criterium.equalList("intValue", 2)));
        assertEquals(p2, repo.selectSingle(TestPrimitiveHolder.class, Criterium.equalList("intValue", 2), null));
        assertNull(repo.selectSingle(TestPrimitiveHolder.class, Criterium.equalList("intValue", 9999), null));

        // fields selection
        assertEquals(Instant.parse("2020-01-01T12:21:11Z"), repo.selectField(TestPrimitiveHolder.class, "ID-2", "createdTs", Instant.class));
        assertArrayEquals(new byte[]{1, 2, 3}, repo.selectField(TestPrimitiveHolder.class, "ID-3", "data", byte[].class));

        assertEquals(Dut.map("createdTs", Instant.parse("2020-01-01T12:21:11Z"), "intValue", 2),
                repo.selectFields(TestPrimitiveHolder.class, "ID-2", Dut.map("createdTs", Instant.class, "intValue", Integer.class)));
    }

    /**
     * Tests update.
     */
    @Test
    public void testUpdate() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestPrimitiveHolder p1 = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 0.1);
        TestPrimitiveHolder p1u = TestPrimitiveHolder.create("ID-1", true, 9, 900L, 0.1);
        TestPrimitiveHolder p2 = TestPrimitiveHolder.create("ID-2", false, 2, 200L, 0.2);
        TestPrimitiveHolder p2u = TestPrimitiveHolder.create("ID-2", true, 5, 200L, 0.5);
        TestPrimitiveHolder p3 = TestPrimitiveHolder.create("ID-3", true, 3, 300L, 0.2);

        repo.insert(p1);
        repo.insert(p2);
        repo.insert(p3);

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));

        assertEquals(p1u, repo.update(p1u));
        assertEquals(Arrays.asList(p1u, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));

        repo.update(TestPrimitiveHolder.class, "ID-2", Dut.map("boolValue", true, "intValue", 5, "doubleValue", 0.5));
        assertEquals(Arrays.asList(p1u, p2u, p3), repo.selectList(TestPrimitiveHolder.class, flt));
    }
    
    /**
     * Tests update when the object doesn't exists.
     */
    @Test
    public void testUpdate_nonExisting() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestPrimitiveHolder p1 = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 0.1);
        TestPrimitiveHolder p1u = TestPrimitiveHolder.create("ID-X", true, 9, 900L, 0.1);
        TestPrimitiveHolder p2 = TestPrimitiveHolder.create("ID-2", false, 2, 200L, 0.2);
        TestPrimitiveHolder p2u = TestPrimitiveHolder.create("ID-2", true, 5, 200L, 0.5);
        TestPrimitiveHolder p3 = TestPrimitiveHolder.create("ID-3", true, 3, 300L, 0.2);

        repo.insert(p1);
        repo.insert(p2);
        repo.insert(p3);

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));

        try {
            repo.update(p1u);
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("TestPrimitiveHolder with id=ID-X does not exists or the nonce does not match"));
        }

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));

        try {
            repo.update(TestPrimitiveHolder.class, "ID-X", Dut.map("boolValue", true, "intValue", 5, "doubleValue", 0.5));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("TestPrimitiveHolder with id=ID-X does not exists or the nonce does not match"));
        }

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));
    }

    /**
     * Tests update with the stamping.
     */
    @Test
    public void testUpdate_stamp() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestStampedHolder p1 = TestStampedHolder.create("ID-1", 1, before2, before2, 1);
        TestStampedHolder p1s = TestStampedHolder.create("ID-1", 1, now, now, 1);
        TestStampedHolder p1u = TestStampedHolder.create("ID-1", 10, before2, before2, 2);
        TestStampedHolder p1us = TestStampedHolder.create("ID-1", 10, before2, now, 2);
        TestStampedHolder p2 = TestStampedHolder.create("ID-2", 2, before1, before1, 1);
        TestStampedHolder p2s = TestStampedHolder.create("ID-2", 2, now, now, 1);
        TestStampedHolder p2us = TestStampedHolder.create("ID-2", 2, now, now, 2);
        TestStampedHolder p2us2 = TestStampedHolder.create("ID-2", 5, now, now, 2);
        TestStampedHolder p3 = TestStampedHolder.create("ID-3", 3, before1, before1, 1);
        TestStampedHolder p3s = TestStampedHolder.create("ID-3", 3, now, now, 1);

        repo.insert(p1);
        repo.insert(p2);
        repo.insert(p3);

        assertEquals(Arrays.asList(p1s, p2s, p3s), repo.selectList(TestStampedHolder.class, flt));

        assertEquals(p1us, repo.update(p1u));
        assertEquals(Arrays.asList(p1us, p2s, p3s), repo.selectList(TestStampedHolder.class, flt));

        repo.update(TestStampedHolder.class, "ID-2", Dut.map("intValue", 5, "nonce", 2L));
        assertEquals(Arrays.asList(p1us, p2us2, p3s), repo.selectList(TestStampedHolder.class, flt));
    }

    /**
     * Tests update with the stamping. This method tests the cases where nonce is wrong (breaking sequence, or missing).
     * In this case, no update should happen.
     */
    @Test
    public void testUpdate_stamp_wrongNonce() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestStampedHolder p1 = TestStampedHolder.create("ID-1", 1, before2, before2, 1);
        TestStampedHolder p1us = TestStampedHolder.create("ID-1", 10, now, now, 2);
        TestStampedHolder p2 = TestStampedHolder.create("ID-2", 2, before1, before1, 1);
        TestStampedHolder p2s = TestStampedHolder.create("ID-2", 2, now, now, 1);

        repo.insert(p1);
        repo.insert(p2);
        repo.update(p1us);

        assertEquals(Arrays.asList(p1us, p2s), repo.selectList(TestStampedHolder.class, flt));

        // nonce lower
        try {
            repo.update(TestStampedHolder.create("ID-1", 5, before1, before1, 1));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("TestStampedHolder with id=ID-1 does not exists or the nonce does not match"));
        }
        assertEquals(Arrays.asList(p1us, p2s), repo.selectList(TestStampedHolder.class, flt));
        
        // nonce equal
        try {
            repo.update(TestStampedHolder.create("ID-1", 5, before1, before1, 2));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("TestStampedHolder with id=ID-1 does not exists or the nonce does not match"));
        }
        assertEquals(Arrays.asList(p1us, p2s), repo.selectList(TestStampedHolder.class, flt));
        
        // nonce skipping
        try {
            repo.update(TestStampedHolder.create("ID-1", 5, before1, before1, 4));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("TestStampedHolder with id=ID-1 does not exists or the nonce does not match"));
        }
        assertEquals(Arrays.asList(p1us, p2s), repo.selectList(TestStampedHolder.class, flt));
        
        
        // nonce doesn't submitted during updated
        try {
            repo.update(TestStampedHolder.class, "ID-1", Dut.map("intValue", 5));
            fail("exception expected");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage(), e.getMessage().contains("nonce is required in the object, but not presented in the update"));
        }
        assertEquals(Arrays.asList(p1us, p2s), repo.selectList(TestStampedHolder.class, flt));
    }
    
    /**
     * Tests deletion.
     */
    @Test
    public void testDelete() {
        Filter flt = new Filter.Builder().
                addAscendantOrder("id").
                build();
        TestPrimitiveHolder p1 = TestPrimitiveHolder.create("ID-1", true, 1, 100L, 0.1);
        TestPrimitiveHolder p2 = TestPrimitiveHolder.create("ID-2", false, 2, 200L, 0.2);
        TestPrimitiveHolder p3 = TestPrimitiveHolder.create("ID-3", true, 3, 300L, 0.2);

        repo.insert(p1);
        repo.insert(p2);
        repo.insert(p3);

        assertEquals(Arrays.asList(p1, p2, p3), repo.selectList(TestPrimitiveHolder.class, flt));

        repo.delete(TestPrimitiveHolder.class, "ID-2");
        assertEquals(Arrays.asList(p1, p3), repo.selectList(TestPrimitiveHolder.class, flt));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
