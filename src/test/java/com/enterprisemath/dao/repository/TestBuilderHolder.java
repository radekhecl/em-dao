package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.Locale;

/**
 * Class which holds builder object.
 *
 * @author radek.hecl
 */
public class TestBuilderHolder {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Identifier.
         */
        private String id;

        /**
         * Human readable name.
         */
        private String name;

        /**
         * Sets the identifier.
         *
         * @param id
         * @return this instance
         */
        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        /**
         * Sets human readable name.
         *
         * @param name name
         * @return this instance
         */
        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        /**
         * Builds the object.
         *
         * @return create object
         */
        public TestBuilderHolder build() {
            return new TestBuilderHolder(this);
        }
    }

    /**
     * Identifier.
     */
    private String id;

    /**
     * Human readable name.
     */
    private String name;

    /**
     * Name in upper case. This is transient field.
     */
    private String nameUpperCase;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    public TestBuilderHolder(Builder builder) {
        id = builder.id;
        name = builder.name;
        guardInvariants();
        nameUpperCase = name.toUpperCase(Locale.US);
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notEmpty(name, "name cannot be empty");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns human readable name.
     *
     * @return human readable name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns name in upper case.
     *
     * @return name in upper case
     */
    public String getNameUpperCase() {
        return nameUpperCase;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }
}
