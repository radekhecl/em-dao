package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.time.Instant;
import static org.apache.ibatis.ognl.OgnlOps.doubleValue;
import static org.apache.ibatis.ognl.OgnlOps.longValue;

/**
 * Test class which holds just primitive types.
 *
 * @author radek.hecl
 */
public class TestStampedHolder {

    /**
     * Added static field to make sure it is ignored.
     */
    public static String STATIC_FIELD_TO_BE_IGNORED = "Hello World";

    /**
     * Identifier.
     */
    private String id;

    /**
     * Integer value.
     */
    private int intValue;

    /**
     * Created timestamp.
     */
    private Instant createdTimestamp;
    
    /**
     * Updated timestamp.
     */
    private Instant updatedTimestamp;
    
    private long nonce;
    
    /**
     * Creates new instance.
     */
    private TestStampedHolder() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmpty(id, "id cannot be empty");
        Guard.notNegative(intValue, "intValue cannot be negative");
        Guard.notNull(createdTimestamp, "createdTimestamp cannot be null");
        Guard.notNull(updatedTimestamp, "updatedTimestamp cannot be null");
        Guard.positive(nonce, "nonce must be positive");
    }

    /**
     * Returns identifier.
     *
     * @return identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Returns integer value.
     *
     * @return integer value
     */
    public int getIntValue() {
        return intValue;
    }

    /**
     * Returns created timestamp.
     * 
     * @return created timestamp
     */
    public Instant getCreatedTimestamp() {
        return createdTimestamp;
    }

    /**
     * Returns updated timestamp.
     * 
     * @return updated timestamp
     */
    public Instant getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    /**
     * Returns nonce.
     * 
     * @return nonce
     */
    public long getNonce() {
        return nonce;
    }

    
    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param id identifier
     * @param intValue int value
     * @param createdTimestamp created timestamp
     * @param updatedTimestamp updated timestamp
     * @param nonce nonce
     * @return crated instance
     */
    public static TestStampedHolder create(String id, int intValue, Instant createdTimestamp, Instant updatedTimestamp, long nonce) {
        TestStampedHolder res = new TestStampedHolder();
        res.id = id;
        res.intValue = intValue;
        res.createdTimestamp = createdTimestamp;
        res.updatedTimestamp = updatedTimestamp;
        res.nonce = nonce;
        res.guardInvariants();
        return res;
    }
}
