package com.enterprisemath.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Before;
import org.junit.Test;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.IdGenerator;
import com.enterprisemath.utils.UuidGenerator;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import static org.junit.Assert.assertEquals;

/**
 * Abstract test for entity dao. Allows all implementation to extend
 * and create exactly same tests for different implementations.
 *
 * @author radek.hecl
 *
 */
public abstract class AbstractRawDaoTest {

    /**
     * Data access object which is tested.
     */
    private RawDao dao;

    /**
     * Test record.
     */
    private RawRecord rec1 = new RawRecord.Builder().
            setField("id", "00000000-0000-0000-0000-000000000001").
            setNullField("null_field").
            setField("bool_field", true).
            setField("int_field", 10).
            setField("long_field", 100L).
            setField("double_field", 10.0d).
            setField("string_field", "string 1").
            setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
            setField("data_field", new byte[]{1, 2, 3}).
            build();

    /**
     * Test record.
     */
    private RawRecord rec2 = new RawRecord.Builder().
            setField("id", "00000000-0000-0000-0000-000000000002").
            setField("null_field", 1).
            setField("bool_field", false).
            setField("int_field", 20).
            setField("long_field", 200L).
            setField("double_field", 20.0d).
            setField("string_field", "string 2").
            setField("datetime_field", LocalDateTime.parse("2020-01-02T17:00:00")).
            setField("data_field", new byte[]{4, 5, 6}).
            build();

    /**
     * Test record.
     */
    private RawRecord rec3 = new RawRecord.Builder().
            setField("id", "00000000-0000-0000-0000-000000000003").
            setNullField("null_field").
            setField("bool_field", false).
            setField("int_field", 10).
            setField("long_field", 300L).
            setField("double_field", 30.0d).
            setField("string_field", "STRING 3").
            setField("datetime_field", LocalDateTime.parse("2020-01-02T17:00:30.123")).
            setField("data_field", new byte[]{7, 8, 9}).
            build();

    /**
     * Fields within the items.
     */
    private Set<String> fields = Dut.immutableSet("id", "null_field", "bool_field", "int_field", "long_field",
            "double_field", "string_field", "datetime_field", "data_field");

    /**
     * Creates new instance.
     */
    public AbstractRawDaoTest() {
    }

    /**
     * Prepares environment.
     */
    @Before
    public final void setUp() {
        dao = getDao();
    }

    /**
     * Returns the dao implementation for test.
     *
     * @return dao implementation for test
     */
    protected abstract RawDao getDao();

    /**
     * Tests single record insertion.
     */
    @Test
    public void testInsert_single() {
        RawRecordIterator iterator = null;

        //
        dao.insert("records", rec1, null);
        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);
        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("datetime_field").
                build(), null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));
    }

    /**
     * Tests single record insertion. This test is to prove that UUID is properly handled.
     * Therefore inserting with random id, and then selecting it.
     * This was the issue with Microsoft SQL Server that stores UUID in a different way.
     */
    @Test
    public void testInsert_single_uuid() {
        IdGenerator uuidGen = UuidGenerator.create();
        List<RawRecord> records = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            RawRecord record = new RawRecord.Builder().
                    setField("id", uuidGen.generateId()).
                    setNullField("null_field").
                    setField("bool_field", true).
                    setField("int_field", i).
                    setField("long_field", 100L).
                    setField("double_field", 10.0d).
                    setField("string_field", "string 1").
                    setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                    setField("data_field", new byte[]{1, 2, 3}).
                    build();
            records.add(record);
            dao.insert("records", record, null);
        }

        RawRecordIterator iterator = null;

        //
        for (RawRecord rec : records) {
            iterator = dao.select("records", fields, new Filter.Builder().
                    addEqualCriterium("id", rec.getObject("id")).
                    addAscendantOrder("id").
                    build(), null);
            assertEquals(Arrays.asList(rec), consume(iterator));
        }
    }

    /**
     * Tests bulk record insertion.
     */
    @Test
    public void testInsert_miltiple() {
        List<RawRecord> records = new ArrayList<>();
        for (int i = 0; i < 230; ++i) {
            RawRecord rec = new RawRecord.Builder().
                    setField("id", "00000000-0000-0000-0000-" + StringUtils.leftPad("" + i, 12, "0")).
                    setNullField("null_field").
                    setField("bool_field", true).
                    setField("int_field", i).
                    setField("long_field", Long.valueOf(i)).
                    setField("double_field", Double.valueOf(i)).
                    setField("string_field", "string " + i).
                    setField("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00").plusSeconds(i)).
                    setField("data_field", new byte[]{(byte) i, (byte) (i + 1), (byte) (i + 2)}).
                    build();
            records.add(rec);
        }

        dao.insert("records", records, null);

        RawRecordIterator iterator = null;

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("datetime_field").
                build(), null);
        assertEquals(records, consume(iterator));
    }

    /**
     * Tests selection and counting.
     */
    @Test
    public void testSelectAndCount() {
        dao.insert("records", rec1, null);
        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);

        Filter filter = null;
        RawRecordIterator iterator = null;

        // test ordering
        filter = new Filter.Builder().
                addAscendantOrder("int_field").
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec3, rec2), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("int_field").
                addDescendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec3, rec1, rec2), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("datetime_field").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));

        filter = new Filter.Builder().
                addDescendantOrder("datetime_field").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec3, rec2, rec1), consume(iterator));

        // test offset and limit
        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(1).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2, rec3), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(3).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Collections.emptyList(), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setLimit(2).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(1).
                setLimit(1).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(1).
                setLimit(2).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2, rec3), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(1).
                setLimit(3).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2, rec3), consume(iterator));

        filter = new Filter.Builder().
                addAscendantOrder("id").
                setOffset(1).
                setLimit(0).
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Collections.emptyList(), consume(iterator));

        //
        // basic criteia
        filter = new Filter.Builder().
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));
        assertEquals(3L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("id", "00000000-0000-0000-0000-000000000001").
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("bool_field", true).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("int_field", 10).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec3), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("long_field", 100L).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("double_field", 10.0).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("string_field", "string 1").
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addInCriterium("id", Dut.set("00000000-0000-0000-0000-000000000001", "00000000-0000-0000-0000-000000000002")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addEqualCriterium("null_field", null).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec3), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addNotEqualCriterium("null_field", null).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        //
        // LocalDateTime criteria
        filter = new Filter.Builder().
                addGreaterOrEqualCriterium("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));
        assertEquals(3L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addGreaterCriterium("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2, rec3), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addLessOrEqualCriterium("datetime_field", LocalDateTime.parse("2020-01-02T17:00:30.123")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));
        assertEquals(3L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addLessCriterium("datetime_field", LocalDateTime.parse("2020-01-02T17:00:30.123")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addGreaterCriterium("datetime_field", LocalDateTime.parse("2020-01-01T14:00:00")).
                addLessCriterium("datetime_field", LocalDateTime.parse("2020-01-02T17:00:30.123")).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2), consume(iterator));
        assertEquals(1L, dao.count("records", filter.getCriteria(), null));

        //
        // pattern criteria
        filter = new Filter.Builder().
                addLikeCriterium("string_field", "%ring%").
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));

        filter = new Filter.Builder().
                addLikeIgnoreCaseCriterium("string_field", "%RinG%").
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec1, rec2, rec3), consume(iterator));
        assertEquals(3L, dao.count("records", filter.getCriteria(), null));

        //
        // big query
        filter = new Filter.Builder().
                addInCriterium("id", createUuidSet(2, 60001)).
                addAscendantOrder("id").
                build();
        iterator = dao.select("records", fields, filter, null);
        assertEquals(Arrays.asList(rec2, rec3), consume(iterator));
        assertEquals(2L, dao.count("records", filter.getCriteria(), null));
    }

    /**
     * Tests update.
     */
    @Test
    public void testUpdate() {
        RawUpdate update1 = new RawUpdate.Builder().
                setNullField("null_field").
                setField("bool_field", true).
                setField("int_field", 25).
                setField("long_field", 205L).
                setField("double_field", 20.5d).
                setField("string_field", "string 2.5").
                setField("datetime_field", LocalDateTime.parse("2021-02-07T19:31:12.999")).
                setField("data_field", new byte[]{9, 10, 11}).
                build();
        RawUpdate update2 = new RawUpdate.Builder().
                setField("string_field", "string 3").
                setField("datetime_field", LocalDateTime.parse("2021-02-07T21:00:00")).
                setField("data_field", new byte[]{13, 15}).
                build();
        RawRecordIterator iterator = null;

        dao.insert("records", rec1, null);
        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);

        //
        assertEquals(1L, dao.update("records", Criterium.equalList("id", "00000000-0000-0000-0000-000000000002"), update1, null));
        assertEquals(0L, dao.update("records", Criterium.equalList("id", "00000000-0000-0000-0000-00000000000a"), update1, null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Arrays.asList(rec1, rec2.update(update1), rec3), consume(iterator));

        //
        assertEquals(2L, dao.update("records", Arrays.asList(Criterium.in(
                "id", Arrays.asList("00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000003"))), update1, null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Arrays.asList(rec1, rec2.update(update1), rec3.update(update1)), consume(iterator));

        //
        assertEquals(3L, dao.update("records", Collections.emptyList(), update2, null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Arrays.asList(rec1.update(update2), rec2.update(update1).update(update2), rec3.update(update1).update(update2)), consume(iterator));
    }

    /**
     * Tests deletion.
     */
    @Test
    public void testDelete() {
        RawRecordIterator iterator = null;

        dao.insert("records", rec1, null);
        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);

        //
        assertEquals(1L, dao.delete("records", Criterium.equalList("id", "00000000-0000-0000-0000-000000000002"), null));
        assertEquals(0L, dao.delete("records", Criterium.equalList("id", "00000000-0000-0000-0000-00000000000a"), null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Arrays.asList(rec1, rec3), consume(iterator));

        dao.insert("records", rec2, null);

        //
        assertEquals(2L, dao.delete("records", Arrays.asList(Criterium.in(
                "id", Arrays.asList("00000000-0000-0000-0000-000000000002", "00000000-0000-0000-0000-000000000003"))), null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Arrays.asList(rec1), consume(iterator));

        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);

        //
        assertEquals(3L, dao.delete("records", Collections.emptyList(), null));

        iterator = dao.select("records", fields, new Filter.Builder().
                addAscendantOrder("id").
                build(), null);
        assertEquals(Collections.emptyList(), consume(iterator));

        dao.insert("records", rec1, null);
        dao.insert("records", rec2, null);
        dao.insert("records", rec3, null);
    }

    /**
     * Consumes iterator to the list of records..
     *
     * @param iterator iterator
     * @return consumed records
     */
    private List<RawRecord> consume(RawRecordIterator iterator) {
        List<RawRecord> res = new ArrayList<>();
        while (iterator.hasNext()) {
            res.add(iterator.getNext());
        }
        return res;
    }

    /**
     * Creates set with UUIDs, starting from 1 till the max.
     *
     * @param max max value (included)
     * @return set with UUIDs
     */
    private Set<String> createUuidSet(int min, int max) {
        String base = "00000000-0000-0000-0000-000000000000";
        Set<String> res = new HashSet<>();
        for (int i = min; i <= max; ++i) {
            String ns = String.valueOf(i);
            String uuid = base.substring(0, base.length() - ns.length()) + ns;
            res.add(uuid);
        }
        return res;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
