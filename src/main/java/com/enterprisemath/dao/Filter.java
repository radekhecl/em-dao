package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Filter object definition.
 *
 * @author radek.hecl
 */
public class Filter {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Criteria.
         */
        private List<Criterium> criteria = new ArrayList<>();

        /**
         * Orders.
         */
        private List<Order> orders = new ArrayList<>();

        /**
         * Offset.
         */
        private long offset = 0;

        /**
         * Limit for the number of records. Null if there is no limit.
         */
        private Integer limit = null;

        /**
         * Sets the specified criteria.
         *
         * @param criteria criteria
         * @return this instance
         */
        public Builder setCriteria(Collection<Criterium> criteria) {
            this.criteria = Dut.copyList(criteria);
            return this;
        }

        /**
         * Adds the specified criterium.
         *
         * @param criterium criterium
         * @return this instance
         */
        public Builder addCriterium(Criterium criterium) {
            criteria.add(criterium);
            return this;
        }

        /**
         * Adds the specified IN criterium.
         *
         * @param field field
         * @param values collection with values
         * @return this instance
         */
        public Builder addInCriterium(String field, Collection<?> values) {
            criteria.add(Criterium.in(field, values));
            return this;
        }

        /**
         * Adds the specified EQUAL criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addEqualCriterium(String field, Object value) {
            criteria.add(Criterium.equal(field, value));
            return this;
        }

        /**
         * Adds the specified NOT_EQUAL criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addNotEqualCriterium(String field, Object value) {
            criteria.add(Criterium.notEqual(field, value));
            return this;
        }

        /**
         * Adds the specified GREATER criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addGreaterCriterium(String field, Object value) {
            criteria.add(Criterium.greater(field, value));
            return this;
        }

        /**
         * Adds the specified GREATER_OR_EQUAL criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addGreaterOrEqualCriterium(String field, Object value) {
            criteria.add(Criterium.greaterOrEqual(field, value));
            return this;
        }

        /**
         * Adds the specified LESS criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addLessCriterium(String field, Object value) {
            criteria.add(Criterium.less(field, value));
            return this;
        }

        /**
         * Adds the specified LESS_OR_EQUAL criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addLessOrEqualCriterium(String field, Object value) {
            criteria.add(Criterium.lessOrEqual(field, value));
            return this;
        }

        /**
         * Adds the specified LIKE criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addLikeCriterium(String field, Object value) {
            criteria.add(Criterium.like(field, value));
            return this;
        }

        /**
         * Adds the specified LIKE_IGNORE_CASE criterium.
         *
         * @param field field
         * @param value value
         * @return this instance
         */
        public Builder addLikeIgnoreCaseCriterium(String field, Object value) {
            criteria.add(Criterium.likeIgnoreCase(field, value));
            return this;
        }

        /**
         * Adds the specified criteria.
         *
         * @param criteria criteria
         * @return this instance
         */
        public Builder addCriteria(Collection<Criterium> criteria) {
            this.criteria.addAll(criteria);
            return this;
        }

        /**
         * Sets the specified orders.
         *
         * @param orders orders
         * @return this instance
         */
        public Builder setOrders(Collection<Order> orders) {
            this.orders = Dut.copyList(orders);
            return this;
        }

        /**
         * Adds the specified order.
         *
         * @param order order
         * @return this instance
         */
        public Builder addOrder(Order order) {
            orders.add(order);
            return this;
        }

        /**
         * Adds the specified ascendant order.
         *
         * @param field for ordering
         * @return this instance
         */
        public Builder addAscendantOrder(String field) {
            orders.add(Order.asc(field));
            return this;
        }

        /**
         * Adds the specified descendant order.
         *
         * @param field for ordering
         * @return this instance
         */
        public Builder addDescendantOrder(String field) {
            orders.add(Order.desc(field));
            return this;
        }

        /**
         * Adds the specified orders.
         *
         * @param orders orders
         * @return this instance
         */
        public Builder addOrders(Collection<Order> orders) {
            this.orders.addAll(orders);
            return this;
        }

        /**
         * Sets selection offset.
         *
         * @param offset selection offset
         * @return this instance
         */
        public Builder setOffset(long offset) {
            this.offset = offset;
            return this;
        }

        /**
         * Sets the limit for selection. Null means unlimited.
         *
         * @param limit limit for selection, null for unlimited
         * @return this instance
         */
        public Builder setLimit(Integer limit) {
            this.limit = limit;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public Filter build() {
            return new Filter(this);
        }
    }

    /**
     * Criteria.
     */
    private List<Criterium> criteria;

    /**
     * Orders.
     */
    private List<Order> orders;

    /**
     * Offset.
     */
    private long offset = 0;

    /**
     * Limit of the selected elements, null for no limit.
     */
    private Integer limit;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private Filter(Builder builder) {
        criteria = Dut.copyImmutableList(builder.criteria);
        orders = Dut.copyImmutableList(builder.orders);
        offset = builder.offset;
        limit = builder.limit;
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNullCollection(criteria, "criteria cannot have null element: %s", criteria);
        Guard.notNullCollection(orders, "orders cannot have null element: %s", orders);
        Guard.notNegative(offset, "offset cannot be negative");
        if (limit != null) {
            Guard.notNull(limit, "limit cannot be negative, if defined");
        }
    }

    /**
     * Returns criteria.
     *
     * @return criteria
     */
    public List<Criterium> getCriteria() {
        return criteria;
    }

    /**
     * Returns orders.
     *
     * @return orders
     */
    public List<Order> getOrders() {
        return orders;
    }

    /**
     * Returns offset.
     *
     * @return offset
     */
    public long getOffset() {
        return offset;
    }

    /**
     * Returns the limit for selection. Null means unlimited.
     *
     * @return limit for selection, null if there is no limit
     */
    public Integer getLimit() {
        return limit;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates a new builder.
     *
     * @return filter builder
     */
    public static Filter.Builder newBuilder() {
        return new Filter.Builder();
    }
}
