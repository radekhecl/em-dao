package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

/**
 * SQL Server implementation.
 *
 * @author radek.hecl
 */
public class SqlServerRawDao implements RawDao {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Factory for sql sessions.
         */
        private SqlSessionFactory sqlSessionFactory;

        /**
         * Sets factory for sql sessions.
         *
         * @param sqlSessionFactory factory for sql sessions
         * @return this instance
         */
        public Builder setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
            this.sqlSessionFactory = sqlSessionFactory;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public SqlServerRawDao build() {
            return new SqlServerRawDao(this);
        }
    }

    /**
     * SQL session factory.
     */
    private SqlSessionFactory sqlSessionFactory;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private SqlServerRawDao(Builder builder) {
        sqlSessionFactory = builder.sqlSessionFactory;
        guardInvariants();
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notNull(sqlSessionFactory, "sqlSessionFactory cannot be null");
    }

    @Override
    public void insert(String collection, RawRecord rec, Map<String, String> options) {
        List<String> fields = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        for (String key : rec.getKeys()) {
            fields.add(key);
            values.add(toMyBatisObject(rec.getObject(key)));
        }
        SqlSession session = sqlSessionFactory.openSession();
        try {
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            mapper.insertRecord(collection, fields, values);
        } finally {
            session.close();
        }
    }

    @Override
    public void insert(String collection, Collection<RawRecord> records, Map<String, String> options) {
        if (records.isEmpty()) {
            return;
        }
        List<RawRecord> lrecs = new ArrayList<>(records);
        Set<String> fkeys = lrecs.get(0).getKeys();
        List<String> fields = Dut.copyList(fkeys);
        List<List<Object>> rawData = new ArrayList<>();
        SqlSession session = sqlSessionFactory.openSession();
        try {
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            for (RawRecord rec : records) {
                Guard.equals(fkeys, rec.getKeys(), "all records must have same fields");
                List<Object> rawRow = new ArrayList<>();
                for (String field : fkeys) {
                    rawRow.add(toMyBatisObject(rec.getObject(field)));
                }
                rawData.add(rawRow);
                if (rawData.size() >= 100) {
                    mapper.insertRecords(collection, fields, rawData);
                    rawData.clear();
                }
            }
            if (!rawData.isEmpty()) {
                mapper.insertRecords(collection, fields, rawData);
                rawData.clear();
            }
        } finally {
            session.close();
        }
    }

    @Override
    public RawRecordIterator select(String collection, Set<String> fields, Filter filter, Map<String, String> options) {
        if (filter.getLimit() != null && filter.getLimit() == 0) {
            return ListRawRecordIterator.create(Collections.emptyList());
        }
        for (Criterium crit : filter.getCriteria()) {
            if (crit.getOperator().equals(Operator.IN)) {
                Collection<?> val = (Collection<?>) crit.getValue();
                if (val == null || val.isEmpty()) {
                    return ListRawRecordIterator.create(Collections.<RawRecord>emptyList());
                }
                if (val.size() > 500) {
                    List<?> valList = Dut.copyList(Dut.copySet(val));
                    List<List<?>> sep = (List<List<?>>) Dut.splitToLists(valList, 500);
                    InMemoryRawDao inMemBeDao = InMemoryRawDao.create();
                    for (List<?> part : sep) {
                        Criterium replacement = Criterium.in(crit.getField(), part);
                        Filter repFilter = partialInCriterium(filter, crit, replacement);
                        RawRecordIterator it = select(collection, fields, repFilter, options);
                        while (it.hasNext()) {
                            inMemBeDao.insert(collection, it.getNext(), options);
                        }
                    }
                    return inMemBeDao.select(collection, fields, filter, options);
                }
            }
        }

        SqlSession session = sqlSessionFactory.openSession();
        try {
            // fetch data
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            List<Map<String, Object>> recs = mapper.selectRecords(collection, Dut.copyList(fields), toMyBatisFilter(filter));
            if (recs == null || recs.isEmpty()) {
                return ListRawRecordIterator.create(Collections.<RawRecord>emptyList());
            }
            // map data
            List<RawRecord> res = new ArrayList<>();
            for (Map<String, Object> rec : recs) {
                rec = keysToLowerCase(rec);
                RawRecord.Builder builder = new RawRecord.Builder();
                for (String field : fields) {
                    Object obj = rec.get(field.toLowerCase(Locale.US));
                    if (obj == null) {
                        builder.setNullField(field);
                    }
                    else if (obj instanceof String) {
                        // SQL server seems that it doesn't support UUID.
                        // Therefore following the convention to convert UUID into a lower case until there is a better known way.
                        String str = (String) obj;
                        if (str.matches("^[A-F0-9]{8}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{4}-[A-F0-9]{12}$")) {
                            str = str.toLowerCase(Locale.US);
                        }
                        builder.setField(field, str);
                    }
                    else if (obj instanceof UUID) {
                        builder.setField(field, obj.toString().toLowerCase(Locale.US));
                    }
                    else if (obj instanceof Boolean) {
                        builder.setField(field, (Boolean) obj);
                    }
                    else if (obj instanceof Integer) {
                        builder.setField(field, (Integer) obj);
                    }
                    else if (obj instanceof Long) {
                        builder.setField(field, (Long) obj);
                    }
                    else if (obj instanceof Float) {
                        builder.setField(field, (Float) obj);
                    }
                    else if (obj instanceof Double) {
                        builder.setField(field, (Double) obj);
                    }
                    else if (obj instanceof java.sql.Timestamp) {
                        java.sql.Timestamp ts = (java.sql.Timestamp) obj;
                        builder.setField(field, ts.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
                    }
                    else if (obj instanceof byte[]) {
                        builder.setField(field, (byte[]) obj);
                    }
                    else {
                        throw new RuntimeException("unsupported JDBC type(" + obj.getClass() + "): " + obj);
                    }
                }
                res.add(builder.build());
            }
            return ListRawRecordIterator.create(res);
        } finally {
            session.close();
        }
    }

    @Override
    public long count(String collection, List<Criterium> criteria, Map<String, String> options) {
        for (Criterium crit : criteria) {
            if (crit.getOperator().equals(Operator.IN)) {
                Collection<?> val = (Collection<?>) crit.getValue();
                if (val == null || val.isEmpty()) {
                    return 0l;
                }
                if (val.size() > 500) {
                    int cidx = criteria.indexOf(crit);
                    List<?> valList = Dut.copyList(Dut.copySet(val));
                    List<List<?>> sep = (List<List<?>>) Dut.splitToLists(valList, 500);
                    long res = 0;
                    for (List<?> part : sep) {
                        List<Criterium> repCrit = Dut.copyList(criteria);
                        repCrit.set(cidx, Criterium.in(crit.getField(), part));
                        res = res + count(collection, repCrit, options);
                    }
                    return res;
                }
            }
        }
        SqlSession session = sqlSessionFactory.openSession();
        try {
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            return mapper.countRecords(collection, toMyBatisCriteria(criteria));
        } finally {
            session.close();
        }
    }

    @Override
    public long update(String collection, List<Criterium> criteria, RawUpdate update, Map<String, String> options) {
        List<Map<String, Object>> fields = new ArrayList<>();
        for (String key : update.getKeys()) {
            fields.add(Dut.map("name", (Object) key, "value", toMyBatisObject(update.getObject(key))));
        }
        SqlSession session = sqlSessionFactory.openSession();
        try {
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            return mapper.updateRecords(collection, toMyBatisCriteria(criteria), fields);
        } finally {
            session.close();
        }
    }

    @Override
    public long delete(String collection, List<Criterium> criteria, Map<String, String> options) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            SqlServerRawMapper mapper = session.getMapper(SqlServerRawMapper.class);
            return mapper.deleteRecords(collection, toMyBatisCriteria(criteria));
        } finally {
            session.close();
        }
    }

    /**
     * Converts object for MyBatis supported object.
     *
     * @param object input object
     * @return result
     */
    private Object toMyBatisObject(Object object) {
        if (object != null) {
            if (object instanceof LocalDateTime) {
                return Date.from(((LocalDateTime) object).atZone(ZoneId.systemDefault()).toInstant());
            }
        }
        return object;
    }

    /**
     * Transforms filter into MyBatis format.
     *
     * @param filter input filter
     * @return transformed filter
     */
    private Filter toMyBatisFilter(Filter filter) {
        Filter.Builder res = new Filter.Builder();
        res.setLimit(filter.getLimit());
        res.setOffset(filter.getOffset());
        res.setOrders(filter.getOrders());
        for (Criterium crit : filter.getCriteria()) {
            res.addCriterium(Criterium.create(crit.getField(), crit.getOperator(), toMyBatisObject(crit.getValue())));
        }
        return res.build();
    }

    /**
     * Transforms criteria into MyBatis format..
     *
     * @param criteria input criteria
     * @return transformed criteria
     */
    private List<Criterium> toMyBatisCriteria(List<Criterium> criteria) {
        List<Criterium> res = new ArrayList<>();
        for (Criterium crit : criteria) {
            res.add(Criterium.create(crit.getField(), crit.getOperator(), toMyBatisObject(crit.getValue())));
        }
        return res;
    }

    /**
     * Converts keys in the input map to the lower case.
     *
     * @param source source map
     * @return map with lower case keys
     */
    private Map<String, Object> keysToLowerCase(Map<String, Object> source) {
        Map<String, Object> res = new HashMap<>(source.size());
        for (String key : source.keySet()) {
            res.put(key.toLowerCase(Locale.US), source.get(key));
        }
        return res;
    }

    /**
     * Creates a filter used as a partial in criterium.
     * This means specified in criterium is replaced, offset is 0 and limit is null.
     *
     * @param input input filter
     * @param criterium criterium to be replaced
     * @param replacement replacement
     * @return filter with replaced criterium
     */
    private Filter partialInCriterium(Filter input, Criterium criterium, Criterium replacement) {
        Filter.Builder res = new Filter.Builder();
        boolean replaced = false;
        for (Criterium crit : input.getCriteria()) {
            if (replaced) {
                res.addCriterium(crit);
            }
            else {
                if (crit.equals(criterium)) {
                    res.addCriterium(replacement);
                    replaced = true;
                }
                else {
                    res.addCriterium(crit);
                }
            }
        }
        res.addOrders(input.getOrders());
        return res.build();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param sqlSessionFactory factory for sql sessions
     * @return created instance
     */
    public static SqlServerRawDao create(SqlSessionFactory sqlSessionFactory) {
        return new SqlServerRawDao.Builder().
                setSqlSessionFactory(sqlSessionFactory).
                build();
    }

}
