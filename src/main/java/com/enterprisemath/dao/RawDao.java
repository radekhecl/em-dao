package com.enterprisemath.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Raw data access layer.
 *
 * <p>
 * <strong>Note:</strong> Adding functions to this interface will <strong>NOT</strong> be
 * considered as breaking binary compatibility.
 *
 * <p>
 * Regarding the different implementations. Although the interface promising to get uniform access across databases,
 * the particular implementations might behave slightly differently. This is caused by the underlined technology.
 * For example databases with schema and free structure are fundamentally different.
 * In addition, for example PostgreSql treats null values and empty string in a different way than Oracle database.
 *
 * @author radek.hecl
 */
public interface RawDao {

    /**
     * Inserts record.
     *
     * @param collection collection to work with
     * @param rec record
     * @param options options in case special behavior is requested, can be null
     */
    public void insert(String collection, RawRecord rec, Map<String, String> options);

    /**
     * Inserts records. This gives the chance to optimize bulk insertion.
     *
     * @param collection collection to work with
     * @param records records to be inserted
     * @param options options in case special behavior is requested, can be null
     */
    public void insert(String collection, Collection<RawRecord> records, Map<String, String> options);

    /**
     * Selects records.
     *
     * @param collection collection to work with
     * @param fields fields to pull
     * @param filter filter
     * @param options options in case special behavior is requested, can be null
     * @return record iterator
     */
    public RawRecordIterator select(String collection, Set<String> fields, Filter filter, Map<String, String> options);

    /**
     * Counts records which satisfies the criteria.
     *
     * @param collection collection to work with
     * @param criteria criteria
     * @param options options in case special behavior is requested, can be null
     * @return number of records, which satisfies the criteria
     */
    public long count(String collection, List<Criterium> criteria, Map<String, String> options);

    /**
     * Updates record.
     *
     * @param collection collection to work with
     * @param criteria criteria to choose the records for update
     * @param update update which will be applied to all records, make sure this is specified, otherwise all records will be updated
     * @param options options in case special behavior is requested, can be null
     * @return number of updated records
     */
    public long update(String collection, List<Criterium> criteria, RawUpdate update, Map<String, String> options);

    /**
     * Deletes record.
     *
     * @param collection collection to work with
     * @param criteria criteria to choose the records to delete, make sure this is specified, otherwise all records will be deleted
     * @param options options in case special behavior is requested, can be null
     * @return number of deleted records
     */
    public long delete(String collection, List<Criterium> criteria, Map<String, String> options);
}
