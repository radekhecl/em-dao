package com.enterprisemath.dao;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper file. Internal use only. Might be changed or removed without notice.
 *
 * @author radek.hecl
 */
public interface HsqlRawMapper {

    /**
     * Inserts record into the table.
     *
     * @param table table
     * @param fields field names
     * @param values values
     */
    public void insertRecord(@Param("table") String table, @Param("fields") List<String> fields, @Param("values") List<Object> values);

    /**
     * Inserts records into the table
     *
     * @param table table
     * @param fields field names
     * @param values record values
     */
    public void insertRecords(@Param("table") String table, @Param("fields") List<String> fields, @Param("values") List<List<Object>> values);

    /**
     * Selects records.
     *
     * @param table table
     * @param fields fields to pull
     * @param filter filter
     * @return selected records
     */
    public List<Map<String, Object>> selectRecords(@Param("table") String table, @Param("fields") List<String> fields, @Param("filter") Filter filter);

    /**
     * Counts records.
     *
     * @param table table
     * @param criteria criteria used for counting
     * @return number of records matching the criteria
     */
    public long countRecords(@Param("table") String table, @Param("criteria") List<Criterium> criteria);

    /**
     * Deletes records
     *
     * @param table table
     * @param criteria criteria which selects records to delete
     * @return number of deleted records
     */
    public int deleteRecords(@Param("table") String table, @Param("criteria") List<Criterium> criteria);

    /**
     * Updates record field.
     *
     * @param table table
     * @param criteria criteria
     * @param fields field value combinations, key is field name, value is
     * @return number of updated records
     */
    public int updateRecords(@Param("table") String table, @Param("criteria") List<Criterium> criteria, @Param("fields") List<Map<String, Object>> fields);
}
