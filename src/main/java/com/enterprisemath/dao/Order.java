package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;

/**
 * Order object definition.
 *
 * @author radek.hecl
 */
public class Order {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Field.
         */
        private String field;

        /**
         * Direction.
         */
        private Direction direction;

        /**
         * Sets the field.
         *
         * @param field field
         * @return this instance
         */
        public Builder setField(String field) {
            this.field = field;
            return this;
        }

        /**
         * Sets the direction.
         *
         * @param direction direction
         * @return this instance
         */
        public Builder setDirection(Direction direction) {
            this.direction = direction;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public Order build() {
            return new Order(this);
        }

    }

    /**
     * Field.
     */
    private String field;

    /**
     * Direction.
     */
    private Direction direction;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private Order(Builder builder) {
        field = builder.field;
        direction = builder.direction;
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notEmpty(field, "field cannot be empty");
        Guard.notNull(direction, "direction cannot be null");
    }

    /**
     * Returns the field.
     *
     * @return field
     */
    public String getField() {
        return field;
    }

    /**
     * Returns the direction.
     *
     * @return direction
     */
    public Direction getDirection() {
        return direction;
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates ascendant order.
     *
     * @param field field
     * @return created object
     */
    public static Order asc(String field) {
        return new Order.Builder().
                setField(field).
                setDirection(Direction.ASCENDANT).
                build();
    }

    /**
     * Creates descendant order.
     *
     * @param field field
     * @return created object
     */
    public static Order desc(String field) {
        return new Order.Builder().
                setField(field).
                setDirection(Direction.DESCENDANT).
                build();
    }

    /**
     * Creates order.
     *
     * @param field field
     * @param direction direction
     * @return created object
     */
    public static Order create(String field, Direction direction) {
        return new Order.Builder().
                setField(field).
                setDirection(direction).
                build();
    }

}
