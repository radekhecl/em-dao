package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Raw update object. Update record holds the fields to be updated.
 * Other fields stays unchanged.
 *
 * @author radek.hecl
 */
public class RawUpdate {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Data fields.
         */
        private Map<String, Object> fields = new HashMap<>();

        /**
         * Sets field to null..
         *
         * @param key key
         * @return this instance
         */
        public Builder setNullField(String key) {
            fields.put(key, null);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, String value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, boolean value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, int value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, long value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, float value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, double value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, LocalDateTime value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, byte[] value) {
            byte[] buf = new byte[value.length];
            System.arraycopy(value, 0, buf, 0, value.length);
            fields.put(key, buf);
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return create object
         */
        public RawUpdate build() {
            return new RawUpdate(this);
        }
    }

    /**
     * Data fields.
     */
    private Map<String, Object> fields;

    /**
     * Creates new instance.
     */
    private RawUpdate() {
    }

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private RawUpdate(Builder builder) {
        fields = Dut.copyImmutableMap(builder.fields);
        guardInvariants();
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmptyStringCollection(fields.keySet(), "fields cannot have an empty key");
    }

    /**
     * Returns the field keys.
     *
     * @return field keys
     */
    public Set<String> getKeys() {
        return fields.keySet();
    }

    /**
     * Returns the field value. Field value is returned as an object.
     *
     * @param key key
     * @return data field value
     * @throws IndexOutOfBoundsException in case data field doesn't exists
     */
    public Object getObject(String key) {
        if (!fields.containsKey(key)) {
            throw new IndexOutOfBoundsException("field doesn't exists: key = " + key);
        }
        Object res = fields.get(key);
        if (res == null) {
            return null;
        }
        else if (res instanceof byte[]) {
            byte[] value = (byte[]) res;
            byte[] buf = new byte[value.length];
            System.arraycopy(value, 0, buf, 0, value.length);
            return buf;
        }
        return res;
    }

    /**
     * Returns the raw update with a given field.
     *
     * @param key key
     * @param value value
     * @return raw update with a given field
     */
    public RawUpdate withField(String key, LocalDateTime value) {
        RawUpdate res = new RawUpdate();
        res.fields = new HashMap<>();
        res.fields.putAll(fields);
        res.fields.put(key, value);
        res.fields = Collections.unmodifiableMap(res.fields);
        res.guardInvariants();
        return res;

    }

    @Override
    public int hashCode() {
        Set<String> ordKeys = new TreeSet<>(fields.keySet());
        int res = 5;
        for (String key : ordKeys) {
            res = res * 13 + 17 * key.hashCode();
            if (fields.get(key) != null) {
                Object f = fields.get(key);
                if (f.getClass().isArray()) {
                    res = res * 13 + 17 * Dut.reflectionHashCode(fields.get(key));
                }
                else {
                    res = res * 13 * 17 * f.hashCode();
                }
            }
        }
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RawUpdate)) {
            return false;
        }
        RawUpdate other = (RawUpdate) obj;
        if (!fields.keySet().equals(other.fields.keySet())) {
            return false;
        }
        for (String key : fields.keySet()) {
            Object tf = fields.get(key);
            Object of = other.fields.get(key);
            if (tf == null && of == null) {
                continue;
            }
            if (tf == null || of == null) {
                return false;
            }
            if (tf.equals(of)) {
                continue;
            }
            if (!Dut.reflectionEquals(fields.get(key), other.fields.get(key))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

}
