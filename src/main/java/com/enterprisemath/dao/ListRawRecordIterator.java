package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Implementation of raw record iterator which iterates through given list of records.
 *
 * @author radek.hecl
 */
public class ListRawRecordIterator implements RawRecordIterator {

    /**
     * Records.
     */
    private List<RawRecord> recs;

    /**
     * Index.
     */
    private int index = 0;

    /**
     * Lock object.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     */
    private ListRawRecordIterator() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNullCollection(recs, "recs cannot have null element");
    }

    @Override
    public boolean hasNext() {
        synchronized (lock) {
            return index < recs.size();
        }
    }

    @Override
    public RawRecord getNext() throws NoSuchElementException {
        synchronized (lock) {
            if (index >= recs.size()) {
                throw new NoSuchElementException("iterator is consumed");
            }
            RawRecord res = recs.get(index);
            index = index + 1;
            return res;
        }
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param recs records to iterate over
     * @return created instance
     */
    public static ListRawRecordIterator create(List<RawRecord> recs) {
        ListRawRecordIterator res = new ListRawRecordIterator();
        res.recs = Dut.copyImmutableList(recs);
        res.guardInvariants();
        return res;
    }

}
