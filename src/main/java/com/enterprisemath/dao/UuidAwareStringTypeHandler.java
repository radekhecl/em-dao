package com.enterprisemath.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;

/**
 * Type handler which is aware of UUID parameters written as a plain String.
 * These parameters are automatically converted to the UUID objects if driver implementation needs it.
 * Depends on configuration and usage of UUID objects this might be necessary to use.
 *
 * @author radek.hecl
 */
@MappedTypes(String.class)
public class UuidAwareStringTypeHandler extends BaseTypeHandler<String> {

    /**
     * Creates new instance.
     */
    public UuidAwareStringTypeHandler() {
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, String parameter, JdbcType jdbcType)
            throws SQLException {
        if (ps.getParameterMetaData().getParameterClassName(i).equals(UUID.class.getName())) {
            ps.setObject(i, UUID.fromString(parameter));
        } else {
            ps.setString(i, parameter);
        }
    }

    @Override
    public String getNullableResult(ResultSet rs, String columnName) throws SQLException {
        return rs.getString(columnName);
    }

    @Override
    public String getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getString(columnIndex);
    }

    @Override
    public String getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return cs.getString(columnIndex);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
