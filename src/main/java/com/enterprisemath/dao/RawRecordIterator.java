package com.enterprisemath.dao;

import java.util.NoSuchElementException;

/**
 * Iterator for the raw records.
 *
 * @author radek.hecl
 */
public interface RawRecordIterator {

    /**
     * Returns whether iterator has next element or not.
     *
     * @return true if iterator
     */
    public boolean hasNext();

    /**
     * Returns the next record.
     *
     * @return next record
     * @throws NoSuchElementException if there are no more records
     */
    public RawRecord getNext() throws NoSuchElementException;

}
