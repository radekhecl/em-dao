package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import java.util.Date;
import java.util.List;

/**
 * Criterium object.
 *
 * @author radek.hecl
 */
public class Criterium {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Field.
         */
        private String field;

        /**
         * Operator.
         */
        private Operator operator;

        /**
         * Value.
         */
        private Object value;

        /**
         * Sets the field.
         *
         * @param field field
         * @return this instance
         */
        public Builder setField(String field) {
            this.field = field;
            return this;
        }

        /**
         * Sets the operator.
         *
         * @param operator operator
         * @return this instance
         */
        public Builder setOperator(Operator operator) {
            this.operator = operator;
            return this;
        }

        /**
         * Sets the value.
         *
         * @param value value
         * @return this instance
         */
        public Builder setValue(Object value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public Criterium build() {
            return new Criterium(this);
        }
    }

    /**
     * Field.
     */
    private String field;

    /**
     * Operator.
     */
    private Operator operator;

    /**
     * Value.
     */
    private Object value;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private Criterium(Builder builder) {
        field = builder.field;
        operator = builder.operator;
        value = copyObject(builder.value);
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(field, "field cannot be null");
        Guard.notNull(operator, "operator cannot be null");
        if (operator == Operator.IN) {
            Guard.beTrue(value instanceof Collection, "value must be collection for IN operator");
        }
    }

    /**
     * Returns the field.
     *
     * @return field
     */
    public String getField() {
        return field;
    }

    /**
     * Returns the operator.
     *
     * @return operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Returns the value.
     *
     * @return value
     */
    public Object getValue() {
        return copyObject(value);
    }

    @Override
    public int hashCode() {
        return Dut.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return Dut.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates the criterium.
     *
     * @param field field
     * @param operator operator
     * @param value value
     * @return created criterium
     */
    public static Criterium create(String field, Operator operator, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(operator).
                setValue(value).
                build();
    }

    /**
     * Creates EQUAL criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium equal(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.EQUAL).
                setValue(value).
                build();
    }

    /**
     * Creates NOT_EQUAL criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium notEqual(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.NOT_EQUAL).
                setValue(value).
                build();
    }

    /**
     * Creates LESS criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium less(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.LESS).
                setValue(value).
                build();
    }

    /**
     * Creates LESS_OR_EQUAL criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium lessOrEqual(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.LESS_OR_EQUAL).
                setValue(value).
                build();
    }

    /**
     * Creates GREATER criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium greater(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.GREATER).
                setValue(value).
                build();
    }

    /**
     * Creates GREATER_OR_EQUAL criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium greaterOrEqual(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.GREATER_OR_EQUAL).
                setValue(value).
                build();
    }

    /**
     * Creates IN criterium.
     *
     * @param field field
     * @param values values
     * @return created criterium
     */
    public static Criterium in(String field, Collection<?> values) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.IN).
                setValue(values).
                build();
    }

    /**
     * Creates LIKE criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium like(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.LIKE).
                setValue(value).
                build();
    }

    /**
     * Creates LIKE_IGNORE_CASE criterium.
     *
     * @param field field
     * @param value value
     * @return created criterium
     */
    public static Criterium likeIgnoreCase(String field, Object value) {
        return new Criterium.Builder().
                setField(field).
                setOperator(Operator.LIKE_IGNORE_CASE).
                setValue(value).
                build();
    }

    /**
     * Creates list containing equal criteria.
     *
     * @param field1 field 1
     * @param value1 value 1
     * @return list of criteria
     */
    public static List<Criterium> equalList(String field1, Object value1) {
        return Arrays.asList(equal(field1, value1));
    }

    /**
     * Creates list containing equal criteria.
     *
     * @param field1 field 1
     * @param value1 value 1
     * @param field2 field 2
     * @param value2 value 2
     * @return list of criteria
     */
    public static List<Criterium> equalList(String field1, Object value1, String field2, Object value2) {
        return Arrays.asList(equal(field1, value1), equal(field2, value2));
    }

    /**
     * Creates list containing equal criteria.
     *
     * @param field1 field 1
     * @param value1 value 1
     * @param field2 field 2
     * @param value2 value 2
     * @param field3 field 3
     * @param value3 value 3
     * @return list of criteria
     */
    public static List<Criterium> equalList(String field1, Object value1, String field2, Object value2, String field3, Object value3) {
        return Arrays.asList(equal(field1, value1), equal(field2, value2), equal(field3, value3));
    }

    /**
     * Returns the copy of the object. This is not the deep copy.
     * Should provide the balance between performance and security.
     * If the source is null, then null will be returned.
     * If the source is date, then the copy will be returned.
     * If the source is list, set or map, then soft copy will be returned.
     * If the source is any other type, then source object will be returned.
     *
     * @param source source object
     * @return copy of the given object
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static Object copyObject(Object source) {
        if (source instanceof Date) {
            return Dut.copyDate((Date) source);
        }
        else if (source instanceof List) {
            return Dut.copyImmutableList((List) source);
        }
        else if (source instanceof Set) {
            return Dut.copyImmutableSet((Set) source);
        }
        else if (source instanceof Map) {
            return Dut.copyImmutableMap((Map) source);
        }
        return source;
    }

}
