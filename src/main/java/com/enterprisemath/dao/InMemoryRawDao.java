package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * In memory implementation for the data access object.
 *
 * @author radek.hecl
 */
public class InMemoryRawDao implements RawDao {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public InMemoryRawDao build() {
            return new InMemoryRawDao(this);
        }
    }

    /**
     * Cache memory.
     */
    private Map<String, List<RawRecord>> cache = new HashMap<>();

    /**
     * Object for synchronization.
     */
    public final Object lock = new Object();

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    public InMemoryRawDao(Builder builder) {
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    @Override
    public void insert(String collection, RawRecord rec, Map<String, String> options) {
        synchronized (lock) {
            if (!cache.containsKey(collection)) {
                cache.put(collection, new ArrayList<>());
            }
            cache.get(collection).add(rec);
        }
    }

    @Override
    public void insert(String collection, Collection<RawRecord> records, Map<String, String> options) {
        for (RawRecord rec : records) {
            insert(collection, rec, options);
        }
    }

    @Override
    public RawRecordIterator select(String collection, Set<String> fields, Filter filter, Map<String, String> options) {
        synchronized (lock) {
            List<RawRecord> filtered = selectByCriteria(collection, filter.getCriteria());
            List<RawRecord> ordered = new ArrayList<>(filtered);
            Collections.sort(ordered, new RrComparator(filter.getOrders()));
            int offset = (int) filter.getOffset();
            int limit = filter.getLimit() == null ? ordered.size() : filter.getLimit();
            List<RawRecord> res = new ArrayList<>(Math.min(limit, ordered.size()));
            for (int i = offset; i < offset + limit && i < ordered.size(); ++i) {
                res.add(ordered.get(i));
            }
            return ListRawRecordIterator.create(res);
        }
    }

    @Override
    public long count(String collection, List<Criterium> criteria, Map<String, String> options) {
        synchronized (lock) {
            return selectByCriteria(collection, criteria).size();
        }
    }

    @Override
    public long update(String collection, List<Criterium> criteria, RawUpdate update, Map<String, String> options) {
        synchronized (lock) {
            List<RawRecord> records = cache.get(collection);
            int res = 0;
            for (int i = 0; i < records.size(); ++i) {
                if (matches(records.get(i), criteria)) {
                    records.set(i, records.get(i).update(update));
                    res = res + 1;
                }
            }
            return res;
        }
    }

    @Override
    public long delete(String collection, List<Criterium> criteria, Map<String, String> options) {
        synchronized (lock) {
            List<RawRecord> records = new ArrayList<>();
            int res = 0;
            for (RawRecord rec : cache.get(collection)) {
                if (matches(rec, criteria)) {
                    res = res + 1;
                }
                else {
                    records.add(rec);
                }
            }
            cache.put(collection, records);
            return res;
        }
    }

    /**
     * Returns set of all collections.
     *
     * @return set of all collections
     */
    public Set<String> getCollections() {
        synchronized (lock) {
            return Dut.copySet(cache.keySet());
        }
    }

    /**
     * Clears all data in this database.
     */
    public void clear() {
        synchronized (lock) {
            cache = new HashMap<>();
        }
    }

    /**
     * Selects records by criteria.
     *
     * @param type type
     * @param criteria criteria
     * @return selected records
     */
    private List<RawRecord> selectByCriteria(String collection, List<Criterium> criteria) {
        List<RawRecord> recs = cache.get(collection);
        if (collection == null) {
            return Collections.emptyList();
        }
        List<RawRecord> res = new ArrayList<>();
        for (RawRecord rec : recs) {
            if (matches(rec, criteria)) {
                res.add(rec);
            }
        }
        return res;
    }

    /**
     * Returns if record matches criteria.
     *
     * @param rec record
     * @param criteria criteria
     * @return true if record matches criteria, false otherwise
     */
    private boolean matches(RawRecord rec, List<Criterium> criteria) {
        for (Criterium crit : criteria) {
            Object critValue = crit.getValue();
            if (critValue != null) {
                if (critValue instanceof Enum<?>) {
                    critValue = ((Enum<?>) critValue).name();
                }
                else if (critValue instanceof Collection<?>) {
                    List<Object> vals = new ArrayList<>();
                    for (Object val : (Collection<?>) critValue) {
                        if (val != null && val instanceof Enum<?>) {
                            vals.add(((Enum<?>) val).name());
                        }
                        else {
                            vals.add(val);
                        }
                    }
                    critValue = vals;
                }

            }

            Object recValue = rec.getObject(crit.getField());

            if (crit.getOperator().equals(Operator.EQUAL)) {
                if (!Dut.safeEquals(recValue, critValue)) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.NOT_EQUAL)) {
                if (Dut.safeEquals(recValue, critValue)) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.IN)) {
                Collection col = (Collection) critValue;
                if (!col.contains(recValue)) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.GREATER)) {
                Guard.beTrue(recValue instanceof Comparable, "value must be Comparable for greater criterium");
                Comparable comparableVal = (Comparable) recValue;
                if (comparableVal.compareTo(critValue) <= 0) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.GREATER_OR_EQUAL)) {
                Guard.beTrue(recValue instanceof Comparable, "value must be Comparable for greater criterium");
                Comparable comparableVal = (Comparable) recValue;
                if (comparableVal.compareTo(critValue) < 0) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.LESS)) {
                Guard.beTrue(recValue instanceof Comparable, "value must be Comparable for greater criterium");
                Comparable comparableVal = (Comparable) recValue;
                if (comparableVal.compareTo(critValue) >= 0) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.LESS_OR_EQUAL)) {
                Guard.beTrue(recValue instanceof Comparable, "value must be Comparable for greater criterium");
                Comparable comparableVal = (Comparable) recValue;
                if (comparableVal.compareTo(critValue) > 0) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.LIKE)) {
                Guard.beTrue(recValue instanceof String, "value must be String for like criterium");
                String exp = (String) critValue;
                exp = exp.replaceAll("%", ".*");
                if (!((String) recValue).matches(exp)) {
                    return false;
                }
            }
            else if (crit.getOperator().equals(Operator.LIKE_IGNORE_CASE)) {
                Guard.beTrue(recValue instanceof String, "value must be String for like ignore case criterium");
                String exp = (String) critValue;
                exp = exp.replaceAll("%", ".*").toLowerCase(Locale.US);
                if (!((String) recValue).toLowerCase(Locale.US).matches(exp)) {
                    return false;
                }
            }
            else {
                throw new RuntimeException("unsupported operator: " + crit);
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static InMemoryRawDao create() {
        return new InMemoryRawDao.Builder().
                build();
    }

    /**
     * Record comparator.
     */
    private static class RrComparator implements Comparator<RawRecord> {

        /**
         * Orders.
         */
        private List<Order> orders;

        /**
         * Creates new instance.
         *
         * @param orders orders
         */
        public RrComparator(List<Order> orders) {
            this.orders = Dut.copyImmutableList(orders);
        }

        @Override
        public int compare(RawRecord o1, RawRecord o2) {
            for (Order order : orders) {
                Object val1 = o1.getObject(order.getField());
                Object val2 = o2.getObject(order.getField());
                Guard.beTrue(val1 instanceof Comparable, "val1 must be comparable");
                Guard.beTrue(val2 instanceof Comparable, "val2 must be comparable");
                Comparable val1Comp = (Comparable) val1;
                Comparable val2Comp = (Comparable) val2;
                int res = val1Comp.compareTo(val2Comp);
                if (res != 0) {
                    if (order.getDirection().equals(Direction.ASCENDANT)) {
                        return res;
                    }
                    else if (order.getDirection().equals(Direction.DESCENDANT)) {
                        return -res;
                    }
                    else {
                        throw new RuntimeException("unknown direction: " + order.getDirection());
                    }
                }
            }
            return 0;
        }

    }

}
