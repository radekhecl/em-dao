package com.enterprisemath.dao;

/**
 * Enumerates the operators which can be used by filter.
 *
 * @author radek.hecl
 */
public enum Operator {

    /**
     * Operator for equality.
     */
    EQUAL,
    /**
     * Operator for non equality.
     */
    NOT_EQUAL,
    /**
     * Less operator.
     */
    LESS,
    /**
     * Less or equal operator.
     */
    LESS_OR_EQUAL,
    /**
     * Greater operator.
     */
    GREATER,
    /**
     * Greater or equal operator.
     */
    GREATER_OR_EQUAL,
    /**
     * In operator.
     */
    IN,
    /**
     * Like operator.
     */
    LIKE,
    /**
     * Ignore case like operator.
     */
    LIKE_IGNORE_CASE

}
