package com.enterprisemath.dao;

/**
 * Enumerates the direction of ordering.
 *
 * @author radek.hecl
 */
public enum Direction {

    /**
     * Ascendant direction. Greatest to the bottom.
     */
    ASCENDANT,
    /**
     * Descendant direction. Greatest to the top.
     */
    DESCENDANT

}
