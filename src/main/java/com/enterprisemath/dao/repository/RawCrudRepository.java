package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Criterium;
import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.RawRecord;
import com.enterprisemath.dao.RawRecordIterator;
import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.enterprisemath.dao.RawDao;

/**
 * Repository realized through the raw dao.
 * All the single objects operation are carried through the field called 'id'.
 * It is up to the class handler to map this into the correct internal field.
 *
 * @author radek.hecl
 */
public class RawCrudRepository implements CrudRepository {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Underlined dao.
         */
        private RawDao rawDao;

        /**
         * Handler between objects and raw records.
         */
        private RawClassHandler handler;

        /**
         * Sets underlined dao.
         *
         * @param rawDao underlined dao
         * @return this instance
         */
        public Builder setRawDao(RawDao rawDao) {
            this.rawDao = rawDao;
            return this;
        }

        /**
         * Sets handler.
         *
         * @param handler handler
         * @return this instance
         */
        public Builder setHandler(RawClassHandler handler) {
            this.handler = handler;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public RawCrudRepository build() {
            return new RawCrudRepository(this);
        }

    }

    /**
     * Underlined dao.
     */
    private RawDao rawDao;

    /**
     * Handler between objects and raw records.
     */
    private RawClassHandler handler;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private RawCrudRepository(Builder builder) {
        rawDao = builder.rawDao;
        handler = builder.handler;
        guardInvariants();
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(rawDao, "rawDao cannot be null");
        Guard.notNull(handler, "handler cannot be null");
    }

    @Override
    public <T> T insert(T object) {
        return insert(object, Collections.<String, Object>emptyMap());
    }

    @Override
    public <T> T insert(T object, Map<String, ? extends Object> fields) {
        String collection = handler.getCollection(object.getClass());
        RawRecord record = handler.toRawRecord(object, fields);
        rawDao.insert(collection, record, null);
        return object;
    }

    @Override
    public <T> List<T> selectList(Class<T> clazz, Filter filter) {
        String collection = handler.getCollection(clazz);
        Set<String> fields = handler.getRawFields(clazz);
        filter = handler.toRawFilter(clazz, filter);
        RawRecordIterator it = rawDao.select(collection, fields, filter, null);
        List<T> res = new ArrayList<>();
        while (it.hasNext()) {
            RawRecord rec = it.getNext();
            res.add(handler.toObject(clazz, rec));
        }
        return res;
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, String id) {
        return selectSingle(clazz, Criterium.equalList("id", id));
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria) {
        Filter filter = new Filter.Builder().
                addCriteria(criteria).
                setOffset(0).
                setLimit(2).
                build();
        List<T> res = selectList(clazz, filter);
        if (res.isEmpty()) {
            throw new IllegalArgumentException(String.format("no record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        if (res.size() > 1) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        return res.get(0);
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, String id, T def) {
        return selectSingle(clazz, Criterium.equalList("id", id), def);
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria, T def) {
        Filter filter = new Filter.Builder().
                addCriteria(criteria).
                setOffset(0).
                setLimit(2).
                build();
        List<T> res = selectList(clazz, filter);
        if (res.isEmpty()) {
            return def;
        }
        if (res.size() > 1) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        return res.get(0);
    }

    @Override
    public <T, U> U selectField(Class<T> clazz, String id, String field, Class<U> fieldClazz) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(clazz);
        RawRecordIterator it = rawDao.select(collection, handler.toRawFields(clazz, Dut.set(field)), filter, null);
        RawRecord rec = it.getNext();
        if (it.hasNext()) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with id = %s", clazz.getName(), id));
        }
        Map<String, Object> map = handler.toObjectFields(clazz, Dut.map(field, fieldClazz), rec);
        return (U) map.get(field);
    }

    @Override
    public <T> Map<String, Object> selectFields(Class<T> clazz, String id, Map<String, ? extends Class<?>> fields) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(clazz);
        RawRecordIterator it = rawDao.select(collection, handler.toRawFields(clazz, fields.keySet()), filter, null);
        RawRecord rec = it.getNext();
        if (it.hasNext()) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with id = %s", clazz.getName(), id));
        }
        return handler.toObjectFields(clazz, fields, rec);
    }

    @Override
    public <T> T update(T object) {
        Filter filter = handler.toRawFilter(object.getClass(), new Filter.Builder().
                addEqualCriterium("id", handler.getIdFieldValue(object)).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(object.getClass());
        long num = rawDao.update(collection, filter.getCriteria(), handler.toRawUpdate(object), null);
        if (num == 0) {
            throw new IllegalArgumentException("object of " + object.getClass() + " with id=" + handler.getIdFieldValue(object) + " does not exists");
        }
        return object;
    }

    @Override
    public <T> void update(Class<T> clazz, String id, Map<String, ? extends Object> fields) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(clazz);
        long num = rawDao.update(collection, filter.getCriteria(), handler.toRawUpdate(clazz, fields), null);
        if (num == 0) {
            throw new IllegalArgumentException("object of " + clazz.getName() + " with id=" + id + " does not exists");
        }
    }

    @Override
    public <T> void delete(Class<T> clazz, String id) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        rawDao.delete(handler.getCollection(clazz), filter.getCriteria(), null);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param rawDao dao layer
     * @param handler handler for object conversion
     * @return created object
     */
    public static RawCrudRepository create(RawDao rawDao, RawClassHandler handler) {
        return new RawCrudRepository.Builder().
                setRawDao(rawDao).
                setHandler(handler).
                build();
    }

}
