package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.RawRecord;
import com.enterprisemath.dao.RawUpdate;
import java.util.Map;
import java.util.Set;

/**
 * Definition of the bridge in between class and raw objects.
 *
 * @author radek.hecl
 */
public interface RawClassHandler {

    /**
     * Returns collections.
     *
     * @param <T> class type
     * @param clazz class object
     * @return collection name
     */
    public <T> String getCollection(Class<T> clazz);

    /**
     * Returns value of the field which is considered as an id field.
     *
     * @param <T> class type
     * @param object object
     * @return value of the field which is considered as an id field
     */
    public <T> Object getIdFieldValue(T object);

    /**
     * Returns the names of the fields used in the raw object.
     *
     * @param <T> class type
     * @param clazz class object
     * @return fields in the raw object
     */
    public <T> Set<String> getRawFields(Class<T> clazz);

    /**
     * Returns the fields used in raw record dao.
     *
     * @param <T> class type
     * @param clazz class object
     * @param fields fields in the object notation
     * @return object fields to query for
     */
    public <T> Set<String> toRawFields(Class<T> clazz, Set<String> fields);

    /**
     * Converts filter from the object notation to the raw record notation.
     *
     * @param <T> class type
     * @param clazz class object
     * @param filter filter in the object notation
     * @return filter in the raw notation
     */
    public <T> Filter toRawFilter(Class<T> clazz, Filter filter);

    /**
     * Converts object to the raw record.
     *
     * @param <T> class type
     * @param object object
     * @param fields additional fields to be converted with the object
     * @return raw record
     */
    public <T> RawRecord toRawRecord(T object, Map<String, ? extends Object> fields);

    /**
     * Converts object to the object for update.
     *
     * @param <T> class type
     * @param object object
     * @return update record
     */
    public <T> RawUpdate toRawUpdate(T object);

    /**
     * Converts given fields to the object for update.
     *
     * @param <T> class type
     * @param clazz class object
     * @param fields fields to convert
     * @return update record
     */
    public <T> RawUpdate toRawUpdate(Class<T> clazz, Map<String, ? extends Object> fields);

    /**
     * Converts raw record to the object.
     *
     * @param <T> class type
     * @param clazz class object
     * @param record raw record
     * @return created object
     */
    public <T> T toObject(Class<T> clazz, RawRecord record);

    /**
     * Converts raw record to the object fields.
     *
     * @param <T> class type
     * @param clazz class object
     * @param fields map of field names in camel notation with types to be returned
     * @param record raw record
     * @return data fields
     */
    public <T> Map<String, Object> toObjectFields(Class<T> clazz, Map<String, ? extends Class<?>> fields, RawRecord record);

}
