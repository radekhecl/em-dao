package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Criterium;
import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.RawRecord;
import com.enterprisemath.dao.RawRecordIterator;
import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.enterprisemath.dao.RawDao;
import com.enterprisemath.dao.RawUpdate;
import com.enterprisemath.utils.NowProvider;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Repository realized through the raw dao.
 * All the single objects operation are carried through the field called 'id'.
 * It is up to the class handler to map this into the correct internal field.
 * <p>
 * In addition, this implementation handles the created timestamp, updated timestamp, out of the box.
 * Also nonce is handled in the way that if it's presented in the class, then it's checked on every update that the number is increased by 1.
 *
 * @author radek.hecl
 */
public class RawStampingCrudRepository implements CrudRepository {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Underlined dao.
         */
        private RawDao rawDao;

        /**
         * Handler between objects and raw records.
         */
        private RawClassHandler handler;

        /**
         * Now provider.
         */
        private NowProvider nowProvider;

        /**
         * Created timestamp filed name.
         */
        private String createdTimstampFld = "createdTimestamp";

        /**
         * Updated timestamp filed name.
         */
        private String updatedTimestampFld = "updatedTimestamp";

        /**
         * Nonce field name.
         */
        private String nonceFld = "nonce";

        /**
         * Sets underlined dao.
         *
         * @param rawDao underlined dao
         * @return this instance
         */
        public Builder setRawDao(RawDao rawDao) {
            this.rawDao = rawDao;
            return this;
        }

        /**
         * Sets handler.
         *
         * @param handler handler
         * @return this instance
         */
        public Builder setHandler(RawClassHandler handler) {
            this.handler = handler;
            return this;
        }

        /**
         * Sets now provider.
         *
         * @param nowProvider now provider
         * @return this instance
         */
        public Builder setNowProvider(NowProvider nowProvider) {
            this.nowProvider = nowProvider;
            return this;
        }

        /**
         * Sets created timestamp field.
         *
         * @param createdTimstampFld created timestamp field in the object notation (usually camel case)
         * @return this instance
         */
        public Builder setCreatedTimstampFld(String createdTimstampFld) {
            this.createdTimstampFld = createdTimstampFld;
            return this;
        }

        /**
         * Sets updated timestamp filed.
         *
         * @param updatedTimestampFld updated timestamp field in the object notation (usually camel case)
         * @return this instance
         */
        public Builder setUpdatedTimestampFld(String updatedTimestampFld) {
            this.updatedTimestampFld = updatedTimestampFld;
            return this;
        }

        /**
         * Sets nonce field.
         *
         * @param nonceFld nonce field in the object notation (usually camel case)
         * @return this instance
         */
        public Builder setNonceFld(String nonceFld) {
            this.nonceFld = nonceFld;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public RawStampingCrudRepository build() {
            return new RawStampingCrudRepository(this);
        }

    }

    /**
     * Underlined dao.
     */
    private RawDao rawDao;

    /**
     * Handler between objects and raw records.
     */
    private RawClassHandler handler;

    /**
     * Now provider.
     */
    private NowProvider nowProvider;

    /**
     * Created timestamp filed name.
     */
    private String createdTimstampFld;

    /**
     * Updated timestamp filed name.
     */
    private String updatedTimestampFld;

    /**
     * Nonce field name.
     */
    private String nonceFld;

    /**
     * Created timestamp filed name in a raw notation.
     */
    private String createdTimstampRawFld;

    /**
     * Updated timestamp filed name in a raw notation.
     */
    private String updatedTimestampRawFld;

    /**
     * Nonce filed name in a raw notation.
     */
    private String nonceRawFld;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private RawStampingCrudRepository(Builder builder) {
        rawDao = builder.rawDao;
        handler = builder.handler;
        nowProvider = builder.nowProvider;
        createdTimstampFld = builder.createdTimstampFld;
        updatedTimestampFld = builder.updatedTimestampFld;
        nonceFld = builder.nonceFld;
        guardInvariants();
        createdTimstampRawFld = Dut.copyList(handler.toRawFields(Object.class, Dut.set(createdTimstampFld))).get(0);
        updatedTimestampRawFld = Dut.copyList(handler.toRawFields(Object.class, Dut.set(updatedTimestampFld))).get(0);
        nonceRawFld = Dut.copyList(handler.toRawFields(Object.class, Dut.set(nonceFld))).get(0);
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Guard.notNull(rawDao, "rawDao cannot be null");
        Guard.notNull(handler, "handler cannot be null");
        Guard.notNull(nowProvider, "nowProvider cannot be null");
        Guard.notEmpty(createdTimstampFld, "createdTimstampFld cannot be empty");
        Guard.notEmpty(updatedTimestampFld, "updatedTimestampFld cannot be empty");
        Guard.notEmpty(nonceFld, "nonceFld cannot be empty");
    }

    @Override
    public <T> T insert(T object) {
        return insert(object, Collections.<String, Object>emptyMap());
    }

    @Override
    public <T> T insert(T object, Map<String, ? extends Object> fields) {
        String collection = handler.getCollection(object.getClass());
        RawRecord record = handler.toRawRecord(object, fields);
        Instant now = nowProvider.instant();
        LocalDateTime utcNow = now.atZone(ZoneId.of("UTC")).toLocalDateTime();
        record = record.update(new RawUpdate.Builder().
                setField(createdTimstampRawFld, utcNow).
                setField(updatedTimestampRawFld, utcNow).
                build());
        rawDao.insert(collection, record, null);
        return handler.toObject((Class<T>) object.getClass(), record);
    }

    @Override
    public <T> List<T> selectList(Class<T> clazz, Filter filter) {
        String collection = handler.getCollection(clazz);
        Set<String> fields = handler.getRawFields(clazz);
        filter = handler.toRawFilter(clazz, filter);
        RawRecordIterator it = rawDao.select(collection, fields, filter, null);
        List<T> res = new ArrayList<>();
        while (it.hasNext()) {
            RawRecord rec = it.getNext();
            res.add(handler.toObject(clazz, rec));
        }
        return res;
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, String id) {
        return selectSingle(clazz, Criterium.equalList("id", id));
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria) {
        Filter filter = new Filter.Builder().
                addCriteria(criteria).
                setOffset(0).
                setLimit(2).
                build();
        List<T> res = selectList(clazz, filter);
        if (res.isEmpty()) {
            throw new IllegalArgumentException(String.format("no record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        if (res.size() > 1) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        return res.get(0);
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, String id, T def) {
        return selectSingle(clazz, Criterium.equalList("id", id), def);
    }

    @Override
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria, T def) {
        Filter filter = new Filter.Builder().
                addCriteria(criteria).
                setOffset(0).
                setLimit(2).
                build();
        List<T> res = selectList(clazz, filter);
        if (res.isEmpty()) {
            return def;
        }
        if (res.size() > 1) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with criteria = %s", clazz.getName(), criteria));
        }
        return res.get(0);
    }

    @Override
    public <T, U> U selectField(Class<T> clazz, String id, String field, Class<U> fieldClazz) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(clazz);
        RawRecordIterator it = rawDao.select(collection, handler.toRawFields(clazz, Dut.set(field)), filter, null);
        RawRecord rec = it.getNext();
        if (it.hasNext()) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with id = %s", clazz.getName(), id));
        }
        Map<String, Object> map = handler.toObjectFields(clazz, Dut.map(field, fieldClazz), rec);
        return (U) map.get(field);
    }

    @Override
    public <T> Map<String, Object> selectFields(Class<T> clazz, String id, Map<String, ? extends Class<?>> fields) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        String collection = handler.getCollection(clazz);
        RawRecordIterator it = rawDao.select(collection, handler.toRawFields(clazz, fields.keySet()), filter, null);
        RawRecord rec = it.getNext();
        if (it.hasNext()) {
            throw new IllegalArgumentException(String.format("more than single record found in %s with id = %s", clazz.getName(), id));
        }
        return handler.toObjectFields(clazz, fields, rec);
    }

    @Override
    public <T> T update(T object) {
        LocalDateTime now = nowProvider.instant().atZone(ZoneId.of("UTC")).toLocalDateTime();
        RawUpdate update = handler.toRawUpdate(object).
                withField(updatedTimestampRawFld, now);
        Filter filter = null;
        if (handler.getRawFields(object.getClass()).contains(nonceRawFld)) {
            long prevNonce = (Long) update.getObject(nonceRawFld) - 1;
            filter = handler.toRawFilter(object.getClass(), new Filter.Builder().
                    addEqualCriterium("id", handler.getIdFieldValue(object)).
                    addEqualCriterium(nonceFld, prevNonce).
                    setOffset(0).
                    setLimit(2).
                    build());
        }
        else {
            filter = handler.toRawFilter(object.getClass(), new Filter.Builder().
                    addEqualCriterium("id", handler.getIdFieldValue(object)).
                    setOffset(0).
                    setLimit(2).
                    build());
        }
        String collection = handler.getCollection(object.getClass());
        long num = rawDao.update(collection, filter.getCriteria(), update, null);
        if (num == 0) {
            throw new IllegalArgumentException("object of " + object.getClass() + " with id=" + handler.getIdFieldValue(object) +
                    " does not exists or the nonce does not match");
        }
        RawRecord rec = handler.toRawRecord(object, Collections.emptyMap());
        rec = rec.update(update);
        object = (T) handler.toObject(object.getClass(), rec);
        return object;
    }

    @Override
    public <T> void update(Class<T> clazz, String id, Map<String, ? extends Object> fields) {
        Filter filter = null;
        if (handler.getRawFields(clazz).contains(nonceRawFld)) {
            if (!fields.containsKey(nonceFld)) {
                throw new IllegalArgumentException("nonce is required in the object, but not presented in the update: " +
                        clazz.getName() + "; " + fields);
            }
            long prevNonce = (Long) fields.get(nonceFld) - 1;
            filter = handler.toRawFilter(clazz, new Filter.Builder().
                    addEqualCriterium("id", id).
                    addEqualCriterium(nonceFld, prevNonce).
                    setOffset(0).
                    setLimit(2).
                    build());
        }
        else {
            filter = handler.toRawFilter(clazz, new Filter.Builder().
                    addEqualCriterium("id", id).
                    setOffset(0).
                    setLimit(2).
                    build());
        }
        String collection = handler.getCollection(clazz);
        long num = rawDao.update(collection, filter.getCriteria(), handler.toRawUpdate(clazz, fields), null);
        if (num == 0) {
            throw new IllegalArgumentException("object of " + clazz + " with id=" + id +
                    " does not exists or the nonce does not match");
        }
    }

    @Override
    public <T> void delete(Class<T> clazz, String id) {
        Filter filter = handler.toRawFilter(clazz, new Filter.Builder().
                addEqualCriterium("id", id).
                setOffset(0).
                setLimit(2).
                build());
        rawDao.delete(handler.getCollection(clazz), filter.getCriteria(), null);
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param rawDao dao layer
     * @param handler handler for object conversion
     * @return created object
     */
    public static RawStampingCrudRepository create(RawDao rawDao, RawClassHandler handler) {
        return new RawStampingCrudRepository.Builder().
                setRawDao(rawDao).
                setHandler(handler).
                build();
    }

}
