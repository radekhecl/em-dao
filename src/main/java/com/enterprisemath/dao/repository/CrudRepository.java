package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Criterium;
import com.enterprisemath.dao.Filter;
import java.util.List;
import java.util.Map;

/**
 * Object related repository. This repository exposes basic CRUD functionality.
 * Advanced features, like concurrent access protection, versioning, or auto logging is not supported by this class.
 *
 * <p>
 * <strong>Note:</strong> Adding functions to this interface will <strong>NOT</strong> be
 * considered as breaking binary compatibility.
 *
 * <p>
 * Convention regarding the text fields in filter, criteria or free fields. All of them should be written in camel case notation.
 * It is responsibility of the implementation to deal with that.s
 *
 * @author radek.hecl
 */
public interface CrudRepository {

    /**
     * Inserts object.
     *
     * @param <T> object type
     * @param object object
     * @return object after insertion
     */
    public <T> T insert(T object);

    /**
     * Inserts object with extra fields.
     *
     * @param <T> object type
     * @param object main object
     * @param fields free fields fields which not included in the main class and should be stored in the same record
     * @return main object after insertion
     */
    public <T> T insert(T object, Map<String, ? extends Object> fields);

    /**
     * Selects list of objects.
     *
     * @param <T> object type
     * @param clazz result object class
     * @param filter filter
     * @return selected objects
     */
    public <T> List<T> selectList(Class<T> clazz, Filter filter);

    /**
     * Selects single object. Throws an exception if there is no object or if there are more than 1 objects.
     *
     * @param <T> object type
     * @param clazz object class
     * @param id id field
     * @return selected object
     */
    public <T> T selectSingle(Class<T> clazz, String id);

    /**
     * Selects single object. Throws an exception if there is no object or if there are more than 1 objects.
     *
     * @param <T> object type
     * @param clazz object class
     * @param criteria criteria which has to select exactly 1 object (otherwise it will be an exception)
     * @return selected object
     */
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria);

    /**
     * Selects single object. Returns default object if there is no object presented in the database.
     * Throws an exception if there is more than 1 object.
     *
     * @param <T> object type
     * @param clazz object class
     * @param id id field value
     * @param def default object
     * @return selected object or default value if object doesn't exists
     */
    public <T> T selectSingle(Class<T> clazz, String id, T def);

    /**
     * Selects single object. Returns default object if there is no object presented in the database.
     * Throws an exception if there is more than 1 object.
     *
     * @param <T> object type
     * @param clazz object class
     * @param criteria criteria which has to select 0 or 1 object (otherwise it will be an exception)
     * @param def default object
     * @return selected object or default value if object doesn't exists
     */
    public <T> T selectSingle(Class<T> clazz, List<Criterium> criteria, T def);

    /**
     * Selects specific object field.
     *
     * @param <T> object type
     * @param <U> field type
     * @param clazz object class
     * @param id identifier
     * @param field field name
     * @param fieldClazz field class which should be returned
     * @return selected fields
     */
    public <T, U> U selectField(Class<T> clazz, String id, String field, Class<U> fieldClazz);

    /**
     * Selects specific object fields.
     *
     * @param <T> object type
     * @param clazz object class
     * @param id identifier
     * @param fields fields to select, keys are the field names, values are classes which should be returned
     * @return selected fields
     */
    public <T> Map<String, Object> selectFields(Class<T> clazz, String id, Map<String, ? extends Class<?>> fields);

    /**
     * Updates whole object.
     *
     * @param <T> object type
     * @param object object
     * @return object after update
     */
    public <T> T update(T object);

    /**
     * Updates specific fields.
     *
     * @param <T> object type
     * @param clazz object class
     * @param id identifier
     * @param fields fields to update
     */
    public <T> void update(Class<T> clazz, String id, Map<String, ? extends Object> fields);

    /**
     * Deletes object.
     *
     * @param <T> object type
     * @param clazz object class
     * @param id identifier
     */
    public <T> void delete(Class<T> clazz, String id);

}
