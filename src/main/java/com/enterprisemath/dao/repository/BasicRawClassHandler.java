package com.enterprisemath.dao.repository;

import com.enterprisemath.dao.Criterium;
import com.enterprisemath.dao.Filter;
import com.enterprisemath.dao.Order;
import com.enterprisemath.dao.RawRecord;
import com.enterprisemath.dao.RawUpdate;
import com.enterprisemath.utils.Dates;
import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import com.enterprisemath.utils.Json;
import com.enterprisemath.utils.StringParser;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Basic raw record class handler.
 * This implementation is designed for a simple and universal usage.
 * The cost for the simplicity is that there are certain preconditions that class has to satisfy.
 *
 * <p>
 * Preconditions.
 * <ul>
 * <li>Class has a string property called 'id', which is a unique object identifier (recommended is to use uuid in a lower case).</li>
 * <li>Property names are written in the camel case.</li>
 * </ul>
 *
 * <p>
 * Regarding collection names. Classes are mapped to the collections according the following rules.
 * <ul>
 * <li>
 * Camel case is turned into underscore. If multiple uppers case letters are following each other, then only first is converted to camel case.
 * After conversion all characters are turned into lower case.
 * </li>
 * <li>Adds 's' to the end if name doesn't end with 'y', 'ss' or 's'.</li>
 * <li>If name ends with 'y', then turns end into 'ies'.</li>
 * <li>If name ends with 'ss', then turns end into 'sses'.</li>
 * <li>If name ends with 's', then does nothing.</li>
 * </ul>
 * <p>
 * Examples:
 * <ul>
 * <li>Car =&gt; cars</li>
 * <li>CarComponent =&gt; car_components</li>
 * <li>Authority =&gt; authorities</li>
 * <li>CarClass =&gt; car_classes</li>
 * <li>CarProperties =&gt; car_properties</li>
 * <li>CarXYZMarket =&gt; car_xyzmarkets</li>
 * <li>XYZ =&gt; xyzs</li>
 * </ul>
 *
 * <p>
 * Regarding the field names. Field names are converted to the raw record fields by changing camel case notation into an underscore notation.
 *
 * @author radek.hecl
 */
public final class BasicRawClassHandler implements RawClassHandler {

    /**
     * Custom type handlers.
     */
    private Map<Class<?>, RawTypeHandler> typeHandlers;

    /**
     * Map with definitions.
     */
    private Map<Class<?>, ClassDefinition> definitions = new ConcurrentHashMap<>();

    /**
     * Field mapping.
     */
    private Map<String, String> mapping = new ConcurrentHashMap<>();

    /**
     * Object for synchronization.
     */
    private final Object lock = new Object();

    /**
     * Creates new instance.
     */
    private BasicRawClassHandler() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notNullMap(typeHandlers, "typeHandlers cannot have null element");
    }

    @Override
    public <T> String getCollection(Class<T> clazz) {
        return getDefinition(clazz).getCollection();
    }

    @Override
    public <T> Object getIdFieldValue(T object) {
        return getDefinition(object.getClass()).invokeGetter(object, "id");
    }

    @Override
    public <T> Set<String> getRawFields(Class<T> clazz) {
        return getDefinition(clazz).getRawFields();
    }

    @Override
    public <T> Set<String> toRawFields(Class<T> clazz, Set<String> fields) {
        return fields.stream().
                map(a -> toRawField(a)).
                collect(Collectors.toSet());
    }

    @Override
    public <T> Filter toRawFilter(Class<T> clazz, Filter filter) {
        Filter.Builder res = new Filter.Builder();
        for (Criterium crit : filter.getCriteria()) {
            String field = toRawField(crit.getField());
            Object val = crit.getValue();
            if (val instanceof Collection) {
                Guard.beTrue((val instanceof List) || (val instanceof Set), "only lists and sets are supported as a collection criterium values");
                Collection<?> col = (Collection<?>) val;
                List<Object> nv = new ArrayList<>();
                for (Object item : col) {
                    nv.add(toRawValue(item));
                }
                val = nv;
            }
            else {
                val = toRawValue(val);
            }
            res.addCriterium(Criterium.create(field, crit.getOperator(), val));
        }
        for (Order ord : filter.getOrders()) {
            String field = toRawField(ord.getField());
            res.addOrder(Order.create(field, ord.getDirection()));
        }
        res.setOffset(filter.getOffset());
        res.setLimit(filter.getLimit());
        return res.build();
    }

    @Override
    public <T> RawRecord toRawRecord(T object, Map<String, ? extends Object> fields) {
        ClassDefinition def = getDefinition(object.getClass());
        Set<String> cfields = def.getClassFields().keySet();
        // check there is no conflict between class and extra fields
        Set<String> intersection = new HashSet<>(cfields);
        intersection.retainAll(fields.keySet());
        Guard.beTrue(intersection.isEmpty(), "there is a conflict in between class and extra fields, following fields are ambiguous: %s", intersection);
        // aggregate all fields
        Map<String, Object> allfields = new HashMap<>();
        allfields.putAll(fields);
        cfields.forEach(f -> allfields.put(f, def.invokeGetter(object, f)));
        // transform all fields
        RawRecord.Builder res = new RawRecord.Builder();
        for (String f : allfields.keySet()) {
            String name = toRawField(f);
            Object val = toRawValue(allfields.get(f));
            if (val == null) {
                res.setNullField(name);
            }
            else if (val instanceof Boolean) {
                res.setField(name, (Boolean) val);
            }
            else if (val instanceof Integer) {
                res.setField(name, (Integer) val);
            }
            else if (val instanceof Long) {
                res.setField(name, (Long) val);
            }
            else if (val instanceof Float) {
                res.setField(name, (Float) val);
            }
            else if (val instanceof Double) {
                res.setField(name, (Double) val);
            }
            else if (val instanceof String) {
                res.setField(name, (String) val);
            }
            else if (val instanceof LocalDateTime) {
                res.setField(name, (LocalDateTime) val);
            }
            else if (val instanceof byte[]) {
                res.setField(name, (byte[]) val);
            }
            else {
                throw new RuntimeException(String.format("unsupported field type %s: %s", val.getClass().getName(), val));
            }
        }
        return res.build();
    }

    @Override
    public <T> RawUpdate toRawUpdate(T object) {
        ClassDefinition def = getDefinition(object.getClass());
        Set<String> cfields = Dut.copySet(def.getClassFields().keySet());
        cfields.remove("id");
        // put fields to map
        Map<String, Object> allfields = new HashMap<>();
        cfields.forEach(f -> allfields.put(f, def.invokeGetter(object, f)));
        // transform all fields
        RawUpdate.Builder res = new RawUpdate.Builder();
        for (String f : allfields.keySet()) {
            String name = toRawField(f);
            Object val = toRawValue(allfields.get(f));
            if (val == null) {
                res.setNullField(name);
            }
            else if (val instanceof Boolean) {
                res.setField(name, (Boolean) val);
            }
            else if (val instanceof Integer) {
                res.setField(name, (Integer) val);
            }
            else if (val instanceof Long) {
                res.setField(name, (Long) val);
            }
            else if (val instanceof Float) {
                res.setField(name, (Float) val);
            }
            else if (val instanceof Double) {
                res.setField(name, (Double) val);
            }
            else if (val instanceof String) {
                res.setField(name, (String) val);
            }
            else if (val instanceof LocalDateTime) {
                res.setField(name, (LocalDateTime) val);
            }
            else if (val instanceof byte[]) {
                res.setField(name, (byte[]) val);
            }
            else {
                throw new RuntimeException(String.format("unsupported field type %s: %s", val.getClass().getName(), val));
            }
        }
        return res.build();
    }

    @Override
    public <T> RawUpdate toRawUpdate(Class<T> clazz, Map<String, ? extends Object> fields) {
        RawUpdate.Builder res = new RawUpdate.Builder();
        for (String f : fields.keySet()) {
            String name = toRawField(f);
            Object val = toRawValue(fields.get(f));
            if (val == null) {
                res.setNullField(name);
            }
            else if (val instanceof Boolean) {
                res.setField(name, (Boolean) val);
            }
            else if (val instanceof Integer) {
                res.setField(name, (Integer) val);
            }
            else if (val instanceof Long) {
                res.setField(name, (Long) val);
            }
            else if (val instanceof Float) {
                res.setField(name, (Float) val);
            }
            else if (val instanceof Double) {
                res.setField(name, (Double) val);
            }
            else if (val instanceof String) {
                res.setField(name, (String) val);
            }
            else if (val instanceof LocalDateTime) {
                res.setField(name, (LocalDateTime) val);
            }
            else if (val instanceof byte[]) {
                res.setField(name, (byte[]) val);
            }
            else {
                throw new RuntimeException(String.format("unsupported field type %s: %s", val.getClass().getName(), val));
            }
        }
        return res.build();
    }

    @Override
    public <T> T toObject(Class<T> clazz, RawRecord record) {
        ClassDefinition def = getDefinition(clazz);
        Map<String, ? extends Class<?>> fields = def.getClassFields();
        Map<String, Object> vals = toObjectFields(clazz, fields, record);
        return (T) def.build(vals);
    }

    @Override
    public <T> Map<String, Object> toObjectFields(Class<T> clazz, Map<String, ? extends Class<?>> fields, RawRecord record) {
        ClassDefinition def = getDefinition(clazz);
        Map<String, Object> res = new HashMap<>();
        for (String cf : fields.keySet()) {
            Class<?> fclazz = fields.get(cf);
            Object val = record.getObject(toRawField(cf));
            if (typeHandlers.containsKey(fclazz)) {
                res.put(cf, typeHandlers.get(fclazz).toObjectType(val));
            }
            else if (val == null) {
                res.put(cf, null);
            }
            else if (fclazz.equals(Boolean.class) || fclazz.equals(boolean.class)) {
                res.put(cf, (Boolean) val);
            }
            else if (fclazz.equals(Integer.class) || fclazz.equals(int.class)) {
                res.put(cf, (Integer) val);
            }
            else if (fclazz.equals(Long.class) || fclazz.equals(long.class)) {
                res.put(cf, (Long) val);
            }
            else if (fclazz.equals(Double.class) || fclazz.equals(double.class)) {
                res.put(cf, (Double) val);
            }
            else if (fclazz.equals(String.class)) {
                res.put(cf, (String) val);
            }
            else if (fclazz.equals(SortedSet.class)) {
                res.put(cf, Dut.copySortedSet(Json.decodeStringList((String) val)));
            }
            else if (fclazz.equals(SortedMap.class)) {
                res.put(cf, Dut.copySortedMap(Json.decodeStringMap((String) val)));
            }
            else if (fclazz.equals(Set.class)) {
                List<?> list = Json.decodeStringList((String) val);
                Class<?> paramClass = def.getClassParameterizedTypeClassNonStrict(cf, 0);
                if (paramClass != null && paramClass.isEnum()) {
                    List<Object> enumList = new ArrayList<>();
                    for (int i = 0; i < list.size(); ++i) {
                        String str = (String) (list.get(i));
                        try {
                            Method m = paramClass.getMethod("valueOf", String.class);
                            Object e = m.invoke(null, str);
                            enumList.add(e);
                        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    list = enumList;
                }
                res.put(cf, Dut.copySet(list));
            }
            else if (fclazz.equals(Map.class)) {
                res.put(cf, Dut.copyMap(Json.decodeStringMap((String) val)));
            }
            else if (fclazz.equals(List.class)) {
                List<?> list = Json.decodeStringList((String) val);
                Class<?> paramClass = def.getClassParameterizedTypeClassNonStrict(cf, 0);
                if (paramClass != null && paramClass.isEnum()) {
                    List<Object> enumList = new ArrayList<>();
                    for (int i = 0; i < list.size(); ++i) {
                        String str = (String) (list.get(i));
                        try {
                            Method m = paramClass.getMethod("valueOf", String.class);
                            Object e = m.invoke(null, str);
                            enumList.add(e);
                        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    list = enumList;
                }
                res.put(cf, list);
            }
            else if (fclazz.equals(Date.class)) {
                LocalDateTime ldt = (LocalDateTime) val;
                res.put(cf, Dates.createTime(ldt.getYear(), ldt.getMonth(), ldt.getDayOfMonth(), ldt.getHour(), ldt.getMinute(),
                        ldt.getSecond(), ldt.getNano() / 1000000));
            }
            else if (fclazz.equals(LocalDateTime.class)) {
                res.put(cf, (LocalDateTime) val);
            }
            else if (fclazz.equals(Instant.class)) {
                LocalDateTime ldt = (LocalDateTime) val;
                res.put(cf, ldt.toInstant(ZoneOffset.UTC));
            }
            else if (fclazz.isEnum()) {
                try {
                    Method m = fclazz.getMethod("valueOf", String.class);
                    Object e = m.invoke(null, val);
                    res.put(cf, (Enum<?>) e);
                } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
            else if (fclazz.equals(byte[].class)) {
                res.put(cf, (byte[]) val);
            }
            else {
                throw new IllegalArgumentException("unsupported fields class: " + fclazz);
            }
        }
        return res;
    }

    /**
     * Returns class definition.
     *
     * @param clazz class
     * @return definition
     */
    private ClassDefinition getDefinition(Class<?> clazz) {
        ClassDefinition res = definitions.get(clazz);
        if (res != null) {
            return res;
        }
        synchronized (lock) {
            res = definitions.get(clazz);
            if (res == null) {
                res = new ClassDefinition(clazz);
                definitions.put(clazz, res);
            }
        }
        return res;
    }

    /**
     * Converts value to the raw value.
     *
     * @param val value in object definition
     * @return value in raw field
     */
    private Object toRawValue(Object val) {
        if (val != null && typeHandlers.containsKey(val.getClass())) {
            val = typeHandlers.get(val.getClass()).toRawType(val);
        }
        if (val == null) {
            return null;
        }
        else if (val instanceof Boolean || val instanceof Integer || val instanceof Long || val instanceof Float || val instanceof Double ||
                val instanceof String) {
            return val;
        }
        else if (val instanceof LocalDateTime) {
            return val;
        }
        else if (val instanceof Instant) {
            Instant instant = (Instant) val;
            return instant.atZone(ZoneId.of("UTC")).toLocalDateTime();
        }
        else if (val instanceof Date) {
            Date dt = (Date) val;
            return LocalDateTime.parse(Dates.format(dt, "yyyy-MM-dd'T'HH:mm:ss.SSS"));
        }
        else if (val instanceof Enum<?>) {
            return ((Enum<?>) val).name();
        }
        else if (val instanceof List) {
            List<?> col = (List<?>) val;
            List<String> strCol = new ArrayList<>();
            for (Object o : col) {
                if (o instanceof Enum<?>) {
                    strCol.add(((Enum< ?>) o).name());
                }
                else {
                    Guard.beTrue(o instanceof String, "only string or enum list is supported");
                    strCol.add((String) o);
                }
            }
            return Json.encodeStringList(strCol);
        }
        else if (val instanceof Set) {
            Set<?> col = (Set<?>) val;
            SortedSet<String> sorted = new TreeSet<>();
            for (Object o : col) {
                if (o instanceof Enum<?>) {
                    sorted.add(((Enum< ?>) o).name());
                }
                else {
                    Guard.beTrue(o instanceof String, "only string or enum set is supported");
                    sorted.add((String) o);
                }
            }
            return Json.encodeStringList(Dut.copyList(sorted));
        }
        else if (val instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) val;
            for (Object k : map.keySet()) {
                Guard.beTrue(k instanceof String, "only string map is supported");
                Guard.beTrue(map.get(k) instanceof String, "only string map is supported");
            }
            return Json.encodeStringMap((Map<String, String>) map);
        }
        else if (val instanceof byte[]) {
            return val;
        }
        else {
            throw new IllegalArgumentException(String.format("unsupported class type %s of %s", val.getClass().getName(), val));
        }
    }

    /**
     * Converts field from class notation to the raw notation.
     *
     * @param classFields class notation
     * @return raw notation
     */
    private String toRawField(String classField) {
        if (mapping.containsKey(classField)) {
            return mapping.get(classField);
        }
        String res = camelToUnderscore(classField);
        mapping.putIfAbsent(classField, res);
        return res;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance with all default settings.
     *
     * @return created instance
     */
    public static BasicRawClassHandler create() {
        BasicRawClassHandler res = new BasicRawClassHandler();
        res.typeHandlers = Collections.emptyMap();
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance with all default settings.
     *
     * @param typeHandlers type handlers
     * @return created instance
     */
    public static BasicRawClassHandler create(Map<Class<?>, RawTypeHandler> typeHandlers) {
        BasicRawClassHandler res = new BasicRawClassHandler();
        res.typeHandlers = Dut.copyImmutableMap(typeHandlers);
        res.guardInvariants();
        return res;
    }

    /**
     * Converts camel case string to underscore notation.
     * As a portion of the process, all characters are converted to the lower case.
     * This has the impact so it's not possible to recover whether first character was upper or lower case.
     * This is purely internal method, do not use outside.
     *
     * @param camel camel case input
     * @return underscore notation
     */
    static String camelToUnderscore(String camel) {
        StringParser parser = new StringParser(camel);
        StringBuilder res = new StringBuilder();
        while (parser.hasNext()) {
            String c = parser.readCharacter();
            if (c.matches("[A-Z]")) {
                if (res.length() > 0) {
                    res.append("_");
                }
                res.append(c.toLowerCase(Locale.US));
            }
            else if (c.matches("[a-z0-9]")) {
                res.append(c);
            }
            else {
                throw new IllegalArgumentException("unsupported string input: " + camel);
            }
        }
        return res.toString();
    }

    /**
     * Converts underscore case string to camel notation.
     * This is purely internal method, do not use outside.
     *
     * @param underscore underscore notation input
     * @return camel case notation, with first character being always lower case
     */
    static String underscoreToCamel(String underscore) {
        StringParser parser = new StringParser(underscore);
        StringBuilder res = new StringBuilder();
        boolean nextUp = false;
        while (parser.hasNext()) {
            String c = parser.readCharacter();
            if (c.equals("_")) {
                nextUp = true;
            }
            else {
                if (nextUp) {
                    res.append(c.toUpperCase(Locale.US));
                    nextUp = false;
                }
                else {
                    res.append(c);
                }
            }
        }
        return res.toString();
    }

    /**
     * Converts string in singular form to the plural form.
     * This is purely internal method, do not use outside.
     *
     * @param str input string, expected is lower case character
     * @return plural form
     */
    static String singularToPlural(String str) {
        if (str.endsWith("ss")) {
            return str + "es";
        }
        else if (str.endsWith("key")) {
            return str + "s";
        }
        else if (str.endsWith("data")) {
            return str;
        }
        else if (str.endsWith("status")) {
            return str + "es";
        }
        else if (str.endsWith("y")) {
            return str.substring(0, str.length() - 1) + "ies";
        }
        else if (str.endsWith("s")) {
            return str;
        }
        else {
            return str + "s";
        }
    }

    /**
     * Class definition with the cached data.
     */
    private static class ClassDefinition {

        /**
         * Class.
         */
        private Class<?> clazz;

        /**
         * Builder class.
         */
        private Class<?> builderClazz;

        /**
         * Collection.
         */
        private String collection;

        /**
         * Field names extracted from class.
         */
        private Map<String, Class<?>> classFields;

        /**
         * Parameterized types for certain fields.
         */
        private Map<String, ParameterizedType> classParametrizedTypes;

        /**
         * Raw field names extracted and translated from the class.
         */
        private Set<String> rawFields;

        /**
         * Getters.
         */
        private Map<String, Method> getters;

        /**
         * Setters.
         */
        private Map<String, Method> setters;

        /**
         * Fields ordered in the factory method.
         */
        private List<String> factoryFields;

        /**
         * Builder constructor.
         */
        private Constructor<?> builderConstructor;

        /**
         * Create method.
         */
        private Method createMethod;

        /**
         * Creates new definition.
         *
         * @param clazz class type
         */
        public ClassDefinition(Class<?> clazz) {
            Guard.notNull(clazz, "clazz cannot be null");
            this.clazz = clazz;
            String sname = clazz.getSimpleName();
            Guard.notEmpty(sname, "anonymous classes are not supported: %s", clazz);
            //
            collection = singularToPlural(camelToUnderscore(sname));

            // extract field names
            classFields = new HashMap<>();
            rawFields = new HashSet<>();
            try {
                builderClazz = Class.forName(clazz.getName() + "$Builder", true, clazz.getClassLoader());
                Field[] fields = builderClazz.getDeclaredFields();
                for (Field f : fields) {
                    if (Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    String name = f.getName();
                    String rawName = camelToUnderscore(name);
                    classFields.put(name, f.getType());
                    rawFields.add(rawName);
                }
            } catch (ClassNotFoundException e) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field f : fields) {
                    if (Modifier.isStatic(f.getModifiers())) {
                        continue;
                    }
                    String name = f.getName();
                    String rawName = camelToUnderscore(name);
                    classFields.put(name, f.getType());
                    rawFields.add(rawName);
                }
            }
            classFields = Dut.copyImmutableMap(classFields);
            rawFields = Dut.copyImmutableSet(rawFields);

            // extract getters
            getters = new HashMap<>();
            for (String fn : classFields.keySet()) {
                Method[] methods = clazz.getMethods();
                Method getter = null;
                for (Method m : methods) {
                    if (m.getName().equals("get" + StringUtils.capitalize(fn)) && m.getParameterCount() == 0) {
                        getter = m;
                        break;
                    }
                    else if (m.getName().equals("is" + StringUtils.capitalize(fn)) && m.getParameterCount() == 0 &&
                            (m.getReturnType() == boolean.class || m.getReturnType() == Boolean.class)) {
                        getter = m;
                        break;
                    }
                }
                Guard.notNull(getter, "unable to find getter for field %s in the class %s", fn, clazz.getName());
                getters.put(fn, getter);
            }
            getters = Dut.copyImmutableMap(getters);

            // extract setters and build methods
            if (builderClazz == null) {
                // look for factory class method
                Method[] methods = clazz.getMethods();
                for (Method m : methods) {
                    if (!m.getName().equals("create")) {
                        continue;
                    }
                    List<Parameter> parameters = Dut.list(m.getParameters());
                    List<String> pnames = parameters.stream().map(p -> p.getName()).collect(Collectors.toList());
                    if (Dut.copySet(pnames).equals(classFields.keySet())) {
                        Type[] genericParamTypes = m.getGenericParameterTypes();
                        Map<String, ParameterizedType> paramTypes = new HashMap();
                        for (int i = 0; i < pnames.size(); ++i) {
                            Type type = genericParamTypes[i];
                            if (type instanceof ParameterizedType) {
                                ParameterizedType ptype = (ParameterizedType) type;
                                paramTypes.put(pnames.get(i), ptype);
                            }
                        }
                        createMethod = m;
                        factoryFields = Dut.copyImmutableList(pnames);
                        classParametrizedTypes = Collections.unmodifiableMap(paramTypes);
                        break;
                    }
                }
                Guard.notNull(createMethod, "unable to find factory method in " + clazz.getName());
            }
            else {
                setters = new HashMap<>();
                for (String fn : classFields.keySet()) {
                    Method[] methods = builderClazz.getMethods();
                    Method setter = null;
                    for (Method m : methods) {
                        if (m.getName().equals("set" + StringUtils.capitalize(fn)) && m.getParameterCount() == 1) {
                            setter = m;
                            break;
                        }
                    }
                    Guard.notNull(setter, "unable to find setter for field %s in the class %s", fn, builderClazz.getName());
                    setters.put(fn, setter);
                }
                setters = Dut.copyImmutableMap(setters);
                // builder constructor
                Constructor<?>[] constructors = builderClazz.getConstructors();
                for (Constructor<?> c : constructors) {
                    if (c.getParameterCount() == 0) {
                        builderConstructor = c;
                        break;
                    }
                }
                Guard.notNull(builderConstructor, "unable to find builder constructor %s", builderClazz.getName());
                // build method
                Method[] methods = builderClazz.getMethods();
                for (Method m : methods) {
                    if (m.getName().equals("build") && m.getParameterCount() == 0) {
                        createMethod = m;
                        break;
                    }
                }
                Guard.notNull(createMethod, "unable to find build method in the class %s", builderClazz.getName());
            }
        }

        /**
         * Returns collection.
         *
         * @return collection
         */
        public String getCollection() {
            return collection;
        }

        /**
         * Returns the class fields.
         *
         * @return class fields
         */
        public Map<String, Class<?>> getClassFields() {
            return classFields;
        }

        /**
         * Returns parameterized types of the class.
         *
         * @return parameterized types of the class
         */
        public Map<String, ParameterizedType> getClassParametrizedTypes() {
            return classParametrizedTypes;
        }

        /**
         * Returns class of the parameterized type of the specified field in a non strict manner.
         * Returns null if this is not possible to discover.
         *
         * @param field field name
         * @param idx index of the parameter
         * @return class of the type parameter, null if this is not possible to discover
         */
        public Class<?> getClassParameterizedTypeClassNonStrict(String field, int idx) {
            try {
                if (!classParametrizedTypes.containsKey(field)) {
                    return null;
                }
                ParameterizedType ptype = classParametrizedTypes.get(field);
                Type type = ptype.getActualTypeArguments()[idx];
                return Class.forName(type.getTypeName());
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Returns raw field names extracted and translated from the class,
         *
         * @return raw field names extracted and translated from the class
         */
        public Set<String> getRawFields() {
            return rawFields;
        }

        /**
         * Invokes getter.
         *
         * @param o object
         * @param field field
         * @return returned value
         */
        public Object invokeGetter(Object o, String field) {
            try {
                Method getter = getters.get(field);
                return getter.invoke(o);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Builds the new instance.
         *
         * @param fields fields
         * @return new instance
         */
        public Object build(Map<String, Object> fields) {
            if (builderClazz == null) {
                Object[] args = new Object[factoryFields.size()];
                for (int i = 0; i < factoryFields.size(); ++i) {
                    args[i] = fields.get(factoryFields.get(i));
                }
                try {
                    return createMethod.invoke(null, args);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
            else {
                try {
                    Object builder = builderConstructor.newInstance();
                    for (String fn : classFields.keySet()) {
                        Method setter = setters.get(fn);
                        setter.invoke(builder, fields.get(fn));
                    }
                    return createMethod.invoke(builder);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }
}
