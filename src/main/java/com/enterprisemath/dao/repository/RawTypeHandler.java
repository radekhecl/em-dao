package com.enterprisemath.dao.repository;

/**
 * Type handler. Converts object types into raw types and back.
 *
 * @author radek.hecl
 */
public interface RawTypeHandler {

    /**
     * Converts object to to the raw type.
     *
     * @param obj object type
     * @return raw type
     */
    public Object toRawType(Object obj);

    /**
     * Converts raw type to the object type.
     *
     * @param raw raw type
     * @return object type
     */
    public Object toObjectType(Object raw);

}
