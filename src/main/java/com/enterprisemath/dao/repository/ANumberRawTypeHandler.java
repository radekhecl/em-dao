package com.enterprisemath.dao.repository;

import com.enterprisemath.utils.ANumber;

/**
 * Type handler that converts ANumber into the string field.
 *
 * @author radek.hecl
 */
public final class ANumberRawTypeHandler implements RawTypeHandler {

    /**
     * Creates new instance.
     */
    private ANumberRawTypeHandler() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
    }

    @Override
    public Object toRawType(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof ANumber) {
            return ((ANumber) obj).toFullString();
        }
        throw new IllegalArgumentException("Only String<->ANumber conversion is supported by this type handler." +
                "This exception means there is a bug in caller's code.");
    }

    @Override
    public Object toObjectType(Object raw) {
        if (raw == null) {
            return null;
        }
        if (raw instanceof String) {
            return ANumber.create((String) raw);
        }
        throw new IllegalArgumentException("Only String<->ANumber conversion is supported by this type handler." +
                "This exception means there is a bug in caller's code.");
    }

    /**
     * Crates new instance.
     *
     * @return created object
     */
    public static ANumberRawTypeHandler create() {
        ANumberRawTypeHandler res = new ANumberRawTypeHandler();
        res.guardInvariants();
        return res;
    }
}
