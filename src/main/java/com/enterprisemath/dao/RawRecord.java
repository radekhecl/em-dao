package com.enterprisemath.dao;

import com.enterprisemath.utils.Dut;
import com.enterprisemath.utils.Guard;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Raw record object. Purpose is to unify and limit data types which needs to work with the data access layer.
 * In that case low level implementations can easy implement this with predictable results.
 *
 * @author radek.hecl
 */
public class RawRecord {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Data fields.
         */
        private Map<String, Object> fields = new HashMap<>();

        /**
         * Sets field to null..
         *
         * @param key key
         * @return this instance
         */
        public Builder setNullField(String key) {
            fields.put(key, null);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, String value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, boolean value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, int value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, long value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, float value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, double value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, LocalDateTime value) {
            fields.put(key, value);
            return this;
        }

        /**
         * Sets field.
         *
         * @param key key
         * @param value value
         * @return this instance
         */
        public Builder setField(String key, byte[] value) {
            byte[] buf = new byte[value.length];
            System.arraycopy(value, 0, buf, 0, value.length);
            fields.put(key, buf);
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return create object
         */
        public RawRecord build() {
            return new RawRecord(this);
        }
    }

    /**
     * Data fields.
     */
    private Map<String, Object> fields;

    /**
     * Creates new instance.
     */
    private RawRecord() {
    }

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private RawRecord(Builder builder) {
        fields = Dut.copyImmutableMap(builder.fields);
        guardInvariants();
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Guard.notEmptyStringCollection(fields.keySet(), "fields cannot have an empty key");
    }

    /**
     * Returns the field keys.
     *
     * @return field keys
     */
    public Set<String> getKeys() {
        return fields.keySet();
    }

    /**
     * Returns the field value. Field value is returned as an object.
     *
     * @param key key
     * @return data field value
     * @throws IndexOutOfBoundsException in case data field doesn't exists
     */
    public Object getObject(String key) {
        if (!fields.containsKey(key)) {
            throw new IndexOutOfBoundsException("field doesn't exists: key = " + key);
        }
        Object res = fields.get(key);
        if (res == null) {
            return null;
        }
        else if (res instanceof byte[]) {
            byte[] value = (byte[]) res;
            byte[] buf = new byte[value.length];
            System.arraycopy(value, 0, buf, 0, value.length);
            return buf;
        }
        return res;
    }

    /**
     * Applies update.
     *
     * @param update update
     * @return record with applied update
     */
    public RawRecord update(RawUpdate update) {
        RawRecord res = new RawRecord();
        Map<String, Object> resf = new HashMap<>();
        for (String key : fields.keySet()) {
            resf.put(key, getObject(key));
        }
        for (String key : update.getKeys()) {
            resf.put(key, update.getObject(key));
        }
        res.fields = Collections.unmodifiableMap(resf);
        res.guardInvariants();
        return res;
    }

    @Override
    public int hashCode() {
        Set<String> ordKeys = new TreeSet<>(fields.keySet());
        int res = 5;
        for (String key : ordKeys) {
            res = res * 13 + 17 * key.hashCode();
            if (fields.get(key) != null) {
                Object f = fields.get(key);
                if (f.getClass().isArray()) {
                    res = res * 13 + 17 * Dut.reflectionHashCode(fields.get(key));
                }
                else {
                    res = res * 13 * 17 * f.hashCode();
                }
            }
        }
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof RawRecord)) {
            return false;
        }
        RawRecord other = (RawRecord) obj;
        if (!fields.keySet().equals(other.fields.keySet())) {
            return false;
        }
        for (String key : fields.keySet()) {
            Object tf = fields.get(key);
            Object of = other.fields.get(key);
            if (tf == null && of == null) {
                continue;
            }
            if (tf == null || of == null) {
                return false;
            }
            if (tf.equals(of)) {
                continue;
            }
            if (!Dut.reflectionEquals(fields.get(key), other.fields.get(key))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return Dut.reflectionToString(this);
    }

}
